<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3de8257a-451e-43a9-969a-c3a8758c745c(mdrs_vil.build)">
  <persistence version="9" />
  <languages>
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="7" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
    <import index="wu98" ref="r:e52567b7-7448-46c1-a824-ca504fa50f0a(build_alef)" />
    <import index="givm" ref="r:e81db3fc-7cdf-47ac-aede-9fc1a5a0aa03(build)" />
    <import index="zwni" ref="r:4c16a3e9-db56-4447-9b0d-14adce23db0d(jetbrains.mps.build.mps.accessories)" implicit="true" />
  </imports>
  <registry>
    <language id="698a8d22-a104-47a0-ba8d-10e3ec237f13" name="jetbrains.mps.build.workflow">
      <concept id="2769948622284574294" name="jetbrains.mps.build.workflow.structure.BwfTaskDependency" flags="ng" index="2VaxJe">
        <reference id="2769948622284574295" name="target" index="2VaxJf" />
      </concept>
      <concept id="2769948622284546675" name="jetbrains.mps.build.workflow.structure.BwfTask" flags="ng" index="2VaFvF">
        <child id="2769948622284574302" name="dependencies" index="2VaxJ6" />
        <child id="2769948622284546679" name="subTasks" index="2VaFvJ" />
      </concept>
      <concept id="2769948622284546677" name="jetbrains.mps.build.workflow.structure.BwfSubTask" flags="ng" index="2VaFvH">
        <child id="2769948622284606050" name="statements" index="2VaTZU" />
      </concept>
      <concept id="2769948622284768359" name="jetbrains.mps.build.workflow.structure.BwfAntStatement" flags="ng" index="2Vbh7Z">
        <child id="2769948622284768360" name="element" index="2Vbh7K" />
      </concept>
    </language>
    <language id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml">
      <concept id="6666499814681541919" name="jetbrains.mps.core.xml.structure.XmlTextValue" flags="ng" index="2pMdtt">
        <property id="6666499814681541920" name="text" index="2pMdty" />
      </concept>
      <concept id="6666499814681415858" name="jetbrains.mps.core.xml.structure.XmlElement" flags="ng" index="2pNNFK">
        <property id="6666499814681415862" name="tagName" index="2pNNFO" />
        <property id="6999033275467544021" name="shortEmptyNotation" index="qg3DV" />
        <child id="6666499814681415861" name="attributes" index="2pNNFR" />
        <child id="1622293396948928802" name="content" index="3o6s8t" />
      </concept>
      <concept id="6666499814681447923" name="jetbrains.mps.core.xml.structure.XmlAttribute" flags="ng" index="2pNUuL">
        <property id="6666499814681447926" name="attrName" index="2pNUuO" />
        <child id="6666499814681541918" name="value" index="2pMdts" />
      </concept>
    </language>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT" />
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="3542413272732620719" name="aspects" index="1hWBAP" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="8654221991637384182" name="jetbrains.mps.build.structure.BuildFileIncludesSelector" flags="ng" index="3qWCbU">
        <property id="8654221991637384184" name="pattern" index="3qWCbO" />
      </concept>
      <concept id="4701820937132281259" name="jetbrains.mps.build.structure.BuildCustomWorkflow" flags="ng" index="1y0Vig">
        <child id="4701820937132281260" name="parts" index="1y0Vin" />
      </concept>
      <concept id="5248329904287794596" name="jetbrains.mps.build.structure.BuildInputFiles" flags="ng" index="3LXTmp">
        <child id="5248329904287794598" name="dir" index="3LXTmr" />
        <child id="5248329904287794679" name="selectors" index="3LXTna" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="6503355885715333289" name="jetbrains.mps.build.mps.structure.BuildMpsAspect" flags="ng" index="2igEWh">
        <property id="7981469545489178349" name="generationMaxHeapSizeInMb" index="3UIfUI" />
      </concept>
      <concept id="1500819558095907805" name="jetbrains.mps.build.mps.structure.BuildMps_Group" flags="ng" index="2G$12M">
        <child id="1500819558095907806" name="modules" index="2G$12L" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <child id="5253498789149547825" name="sources" index="3bR31x" />
        <child id="5253498789149547704" name="dependencies" index="3bR37C" />
      </concept>
      <concept id="5253498789149585690" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyOnModule" flags="ng" index="3bR9La">
        <reference id="5253498789149547705" name="module" index="3bR37D" />
      </concept>
      <concept id="4297162197620964123" name="jetbrains.mps.build.mps.structure.BuildMps_GeneratorOptions" flags="ng" index="1wNqPr" />
      <concept id="4278635856200817744" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleModelRoot" flags="ng" index="1BupzO">
        <property id="8137134783396907368" name="convert2binary" index="1Hdu6h" />
        <property id="8137134783396676838" name="extracted" index="1HemKv" />
        <property id="2889113830911481881" name="deployFolderName" index="3ZfqAx" />
        <child id="8137134783396676835" name="location" index="1HemKq" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
      <concept id="7259033139236285166" name="jetbrains.mps.build.mps.structure.BuildMps_ExtractedModuleDependency" flags="nn" index="1SiIV0">
        <child id="7259033139236285167" name="dependency" index="1SiIV1" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="_tBehZDXfT">
    <property role="TrG5h" value="mdrs_vil" />
    <property role="2DA0ip" value="../../" />
    <node concept="1y0Vig" id="1C1cdGYVeR3" role="1hWBAP">
      <node concept="2VaFvF" id="1C1cdGYVeR7" role="1y0Vin">
        <property role="TrG5h" value="migrate" />
        <node concept="2VaxJe" id="1C1cdGYVeRd" role="2VaxJ6">
          <ref role="2VaxJf" to="zwni:m8_23b_6ft" resolve="declare-mps-tasks" />
        </node>
        <node concept="2VaFvH" id="1C1cdGYVeRg" role="2VaFvJ">
          <property role="TrG5h" value="migrations" />
          <node concept="2Vbh7Z" id="1C1cdGYVeRi" role="2VaTZU">
            <node concept="2pNNFK" id="1C1cdGYVeRm" role="2Vbh7K">
              <property role="2pNNFO" value="migrate" />
              <node concept="2pNNFK" id="1C1cdGYVeS2" role="3o6s8t">
                <property role="2pNNFO" value="repository" />
                <node concept="2pNNFK" id="1C1cdGYVeSd" role="3o6s8t">
                  <property role="2pNNFO" value="allmpsmodules" />
                  <property role="qg3DV" value="true" />
                </node>
                <node concept="2pNNFK" id="1C1cdGYVeSo" role="3o6s8t">
                  <property role="2pNNFO" value="modules" />
                  <node concept="2pNUuL" id="1C1cdGYVeSw" role="2pNNFR">
                    <property role="2pNUuO" value="dir" />
                    <node concept="2pMdtt" id="1C1cdGYVeSx" role="2pMdts">
                      <property role="2pMdty" value="${basedir}/solutions" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="1C1cdGYVeRu" role="2pNNFR">
                <property role="2pNUuO" value="project" />
                <node concept="2pMdtt" id="1C1cdGYVeRv" role="2pMdts">
                  <property role="2pMdty" value="${basedir}" />
                </node>
              </node>
              <node concept="2pNUuL" id="1C1cdGYVeR$" role="2pNNFR">
                <property role="2pNUuO" value="mpshome" />
                <node concept="2pMdtt" id="1C1cdGYVeR_" role="2pMdts">
                  <property role="2pMdty" value="${alef.home}" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1wNqPr" id="1C1cdGYVeQu" role="3989C9" />
    <node concept="2igEWh" id="7rrag0nGWo5" role="1hWBAP">
      <property role="3UIfUI" value="3072" />
    </node>
    <node concept="10PD9b" id="_tBehZDXfU" role="10PD9s" />
    <node concept="3b7kt6" id="_tBehZDXfV" role="10PD9s" />
    <node concept="398rNT" id="4VBt1tqCpbO" role="1l3spd">
      <property role="TrG5h" value="alef.home" />
    </node>
    <node concept="2sgV4H" id="439w2OT829T" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="439w2OT829U" role="2JcizS">
        <ref role="398BVh" node="4VBt1tqCpbO" resolve="alef.home" />
      </node>
    </node>
    <node concept="2sgV4H" id="3lFOqBdVXgQ" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="3lFOqBdWdaC" role="2JcizS">
        <ref role="398BVh" node="4VBt1tqCpbO" resolve="alef.home" />
        <node concept="2Ry0Ak" id="XPk7SeI$B_" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="3lFOqBdVWMR" role="1l3spa">
      <ref role="1l3spb" to="wu98:4wvGeDpkGQL" resolve="alef-plugin" />
      <node concept="398BVA" id="3lFOqBdWdau" role="2JcizS">
        <ref role="398BVh" node="4VBt1tqCpbO" resolve="alef.home" />
        <node concept="2Ry0Ak" id="XPk7SeIywJ" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="7Z74QVcKzJF" role="1l3spa">
      <ref role="1l3spb" to="givm:50VLgx6DlzG" resolve="linguistics" />
      <node concept="398BVA" id="7Z74QVcKzK5" role="2JcizS">
        <ref role="398BVh" node="4VBt1tqCpbO" resolve="alef.home" />
        <node concept="2Ry0Ak" id="7Z74QVcKzK6" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="1l3spV" id="_tBehZDXgw" role="1l3spN" />
    <node concept="2G$12M" id="_tBehZDXgi" role="3989C9">
      <property role="TrG5h" value="Alef" />
      <node concept="1E1JtA" id="1C1cdGYVeLa" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="VILSpecificatie" />
        <property role="3LESm3" value="32b5edc6-e334-4946-bc4a-3471045a4e9c" />
        <node concept="55IIr" id="1C1cdGYVeLd" role="3LF7KH">
          <node concept="2Ry0Ak" id="1C1cdGYWF3q" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="1C1cdGYWF3v" role="2Ry0An">
              <property role="2Ry0Am" value="VILSpecificatie" />
              <node concept="2Ry0Ak" id="1C1cdGYWF3$" role="2Ry0An">
                <property role="2Ry0Am" value="VILSpecificatie.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1BupzO" id="1C1cdGYVeN9" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="1C1cdGYVeNa" role="1HemKq">
            <node concept="55IIr" id="1C1cdGYVeN5" role="3LXTmr">
              <node concept="2Ry0Ak" id="1C1cdGYVeN6" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="1C1cdGYVeN7" role="2Ry0An">
                  <property role="2Ry0Am" value="VILSpecificatie" />
                  <node concept="2Ry0Ak" id="1C1cdGYVeN8" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="1C1cdGYVeNb" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="1C1cdGYVeO2" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="VILTests" />
        <property role="3LESm3" value="97bfcbab-d52e-4309-9f6b-7d5f60cbab49" />
        <node concept="55IIr" id="1C1cdGYVeO5" role="3LF7KH">
          <node concept="2Ry0Ak" id="1C1cdGYWF3D" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="1C1cdGYWF3I" role="2Ry0An">
              <property role="2Ry0Am" value="VILTests" />
              <node concept="2Ry0Ak" id="1C1cdGYWF3N" role="2Ry0An">
                <property role="2Ry0Am" value="VILTests.msd" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="1C1cdGYVePw" role="3bR37C">
          <node concept="3bR9La" id="1C1cdGYVePx" role="1SiIV1">
            <ref role="3bR37D" node="1C1cdGYVeLa" resolve="VILSpecificatie" />
          </node>
        </node>
        <node concept="1BupzO" id="1C1cdGYVePA" role="3bR31x">
          <property role="3ZfqAx" value="models" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="1C1cdGYVePB" role="1HemKq">
            <node concept="55IIr" id="1C1cdGYVePy" role="3LXTmr">
              <node concept="2Ry0Ak" id="1C1cdGYVePz" role="iGT6I">
                <property role="2Ry0Am" value="solutions" />
                <node concept="2Ry0Ak" id="1C1cdGYVeP$" role="2Ry0An">
                  <property role="2Ry0Am" value="VILTests" />
                  <node concept="2Ry0Ak" id="1C1cdGYVeP_" role="2Ry0An">
                    <property role="2Ry0Am" value="models" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="1C1cdGYVePC" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

