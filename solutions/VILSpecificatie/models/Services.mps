<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d4c0de40-e3df-4f3d-b55d-4f7e70dbeb0b(Services)">
  <persistence version="9" />
  <languages>
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="6mfl" ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Termen)" />
    <import index="xshh" ref="r:e66d6402-1c81-4d02-b573-4fd1d2d735a4(Besturing)" />
    <import index="35ve" ref="r:9323a62c-c9bd-4ca3-a074-f1b4670ff741(Parameters)" />
  </imports>
  <registry>
    <language id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml">
      <concept id="6666499814681515200" name="jetbrains.mps.core.xml.structure.XmlFile" flags="ng" index="2pMbU2">
        <child id="6666499814681515201" name="document" index="2pMbU3" />
      </concept>
      <concept id="6666499814681541919" name="jetbrains.mps.core.xml.structure.XmlTextValue" flags="ng" index="2pMdtt">
        <property id="6666499814681541920" name="text" index="2pMdty" />
      </concept>
      <concept id="6666499814681299057" name="jetbrains.mps.core.xml.structure.XmlProlog" flags="ng" index="2pNm8N">
        <child id="7604553062773674214" name="elements" index="BGLLu" />
      </concept>
      <concept id="6666499814681415858" name="jetbrains.mps.core.xml.structure.XmlElement" flags="ng" index="2pNNFK">
        <property id="6666499814681415862" name="tagName" index="2pNNFO" />
        <property id="6999033275467544021" name="shortEmptyNotation" index="qg3DV" />
        <child id="6666499814681415861" name="attributes" index="2pNNFR" />
        <child id="1622293396948928802" name="content" index="3o6s8t" />
      </concept>
      <concept id="6666499814681447923" name="jetbrains.mps.core.xml.structure.XmlAttribute" flags="ng" index="2pNUuL">
        <property id="6666499814681447926" name="attrName" index="2pNUuO" />
        <child id="6666499814681541918" name="value" index="2pMdts" />
      </concept>
      <concept id="6786756355279841993" name="jetbrains.mps.core.xml.structure.XmlDocument" flags="ng" index="3rIKKV">
        <child id="6666499814681299055" name="rootElement" index="2pNm8H" />
        <child id="6666499814681299060" name="prolog" index="2pNm8Q" />
      </concept>
      <concept id="5228786488744996718" name="jetbrains.mps.core.xml.structure.XmlDeclaration" flags="ng" index="3W$oVP">
        <property id="5491461270226117667" name="version" index="1D$jbd" />
      </concept>
    </language>
    <language id="51f8c68a-90ac-4dbf-b58a-c9e9db784c81" name="xml.schema">
      <concept id="5695998258397210558" name="xml.schema.structure.XsdProlog" flags="ng" index="2e3yu2" />
      <concept id="4739644308928091986" name="xml.schema.structure.XmlSchemaFile" flags="ng" index="1yAGSL" />
      <concept id="2657656834095411304" name="xml.schema.structure.XsdNameReference" flags="ng" index="1JL9iB">
        <property id="2657656834095774006" name="attrName" index="1JMLRT" />
        <reference id="2657656834095411305" name="ref" index="1JL9iA" />
      </concept>
      <concept id="2657656834095605320" name="xml.schema.structure.XsdNamedElement" flags="ng" index="1JMoa7" />
    </language>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="8878823228840241647" name="gegevensspraak.structure.TekstType" flags="ng" index="THod0" />
      <concept id="5917060184181247441" name="gegevensspraak.structure.BooleanType" flags="ng" index="1EDDcM" />
      <concept id="5917060184181247285" name="gegevensspraak.structure.DomeinType" flags="ng" index="1EDDfm">
        <reference id="5917060184181247286" name="domein" index="1EDDfl" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="d8af31be-1847-4d5b-8686-78e232d4e0f8" name="servicespraak">
      <concept id="3470082797177816366" name="servicespraak.structure.RestrictedDatatype" flags="ng" index="2OB35">
        <child id="3470082797177809500" name="restricties" index="2OxYR" />
      </concept>
      <concept id="3470082797177568556" name="servicespraak.structure.PredefinedBerichtDataType" flags="ng" index="2R$z7">
        <property id="3470082797177593129" name="predef" index="2RIz2" />
      </concept>
      <concept id="3470082797177561953" name="servicespraak.structure.BerichtDatatypeDefinition" flags="ng" index="2R_qa">
        <child id="8943333957934572437" name="base" index="2Evv_c" />
      </concept>
      <concept id="3470082797188803840" name="servicespraak.structure.DecimalenRestrictie" flags="ng" index="3ytzF">
        <property id="3470082797188803843" name="cijfersTotaal" index="3ytzC" />
        <property id="3470082797188803844" name="achterKomma" index="3ytzJ" />
      </concept>
      <concept id="1482961590271922253" name="servicespraak.structure.BerichtType" flags="ng" index="2785BS">
        <reference id="2657656834086768020" name="object" index="1Ig6_r" />
        <child id="1482961590271922261" name="veld" index="2785Bw" />
      </concept>
      <concept id="1482961590271922255" name="servicespraak.structure.UitvoerBerichtType" flags="ng" index="2785BU" />
      <concept id="1482961590271922254" name="servicespraak.structure.InvoerBerichtType" flags="ng" index="2785BV" />
      <concept id="2142746120988295507" name="servicespraak.structure.Service" flags="ng" index="2kTx5H">
        <property id="578084289618712443" name="xmlBerichtUitType" index="OWdfy" />
        <property id="578084289618712437" name="xmlBerichtInType" index="OWdfG" />
        <property id="578084289618604529" name="xmlBerichtType" index="OXFlC" />
        <property id="578084289618529498" name="xmlRekenmomentVeld" index="OXSx3" />
        <property id="6333744389222434815" name="xsdVersie" index="2QnZO9" />
        <property id="8879350159213016781" name="componentnaam" index="2R2JWx" />
        <property id="8879350159213016767" name="projectnaam" index="2R2JXj" />
        <reference id="2142746120988295510" name="startFlow" index="2kTx5C" />
        <child id="7010317595068611080" name="mapping" index="21XpMX" />
        <child id="3470082797177933968" name="datatype" index="2P2lV" />
        <child id="8244924399861867485" name="paramset" index="2nR7zj" />
        <child id="8880636053083745918" name="invoer" index="KENke" />
        <child id="8880636053083745928" name="uitvoer" index="KENnS" />
      </concept>
      <concept id="8244924399861867440" name="servicespraak.structure.ParameterSetRef" flags="ng" index="2nR7yY">
        <reference id="8244924399861867441" name="ref" index="2nR7yZ" />
      </concept>
      <concept id="8880636053084576132" name="servicespraak.structure.ComplexInvoerBerichtVeld" flags="ng" index="KB4bO" />
      <concept id="8880636053084603182" name="servicespraak.structure.ComplexUitvoerBerichtVeld" flags="ng" index="KBdxu" />
      <concept id="8880636053083348511" name="servicespraak.structure.ComplexBerichtVeld" flags="ng" index="KGglJ">
        <reference id="8880636053083348520" name="sub" index="KGglo" />
      </concept>
      <concept id="5182003326601264810" name="servicespraak.structure.Xsd" flags="ng" index="2P7X8V">
        <property id="5182003326601509062" name="published" index="2P4Thn" />
        <property id="5182003326601303501" name="versie" index="2P7b_s" />
        <property id="5182003326601315407" name="generatie" index="2P7ezu" />
        <reference id="5182003326601285643" name="service" index="2P77Mq" />
        <child id="5182003326604458218" name="xsdContent" index="2ON9hV" />
      </concept>
      <concept id="3670915702568119714" name="servicespraak.structure.BerichtDataTypeRef" flags="ng" index="3x25J3">
        <reference id="3670915702568123411" name="ref" index="3x24DM" />
      </concept>
      <concept id="3670915702568636606" name="servicespraak.structure.DataTypeMapping" flags="ng" index="3AW6rv">
        <child id="8847999994590430363" name="extern" index="2KWIQ6" />
        <child id="3670915702568638455" name="intern" index="3AW66m" />
      </concept>
      <concept id="2657656834081800124" name="servicespraak.structure.DirectInvoerAttribuut" flags="ng" index="1IH5HN" />
      <concept id="2657656834081829135" name="servicespraak.structure.DirectUitvoerAttribuut" flags="ng" index="1IHXn0" />
      <concept id="2657656834082458620" name="servicespraak.structure.DirectAttribuut" flags="ng" index="1IJyWN">
        <property id="4695460247159644351" name="verplicht" index="3pKC28" />
        <reference id="2657656834082458621" name="attr" index="1IJyWM" />
      </concept>
    </language>
  </registry>
  <node concept="2785BV" id="6PDUWJHkXWs">
    <property role="TrG5h" value="Invoer_Aanvraag_IIT" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <ref role="1Ig6_r" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1IH5HN" id="6PDUWJHkXWt" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXWy" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXWE" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXWO" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXX0" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXXe" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
    </node>
    <node concept="1IH5HN" id="6PDUWJHkXXu" role="2785Bw">
      <property role="3pKC28" value="true" />
      <ref role="1IJyWM" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
    </node>
  </node>
  <node concept="2785BU" id="6PDUWJHkXXB">
    <property role="TrG5h" value="Uitvoer_Besluit_IIT" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <ref role="1Ig6_r" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1IHXn0" id="6PDUWJHkXXC" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
    </node>
    <node concept="1IHXn0" id="6PDUWJHkXXH" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
    </node>
  </node>
  <node concept="2kTx5H" id="6PDUWJHl8uC">
    <property role="OXFlC" value="Message" />
    <property role="OWdfG" value="Request" />
    <property role="OWdfy" value="Response" />
    <property role="OXSx3" value="belastingjaar" />
    <property role="TrG5h" value="IIT" />
    <property role="2R2JXj" value="poc" />
    <property role="2R2JWx" value="iit" />
    <property role="2QnZO9" value="0.3" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <ref role="2kTx5C" to="xshh:6PDUWJHkRWZ" resolve="Bepalen individuele inkomenstoeslag" />
    <node concept="KB4bO" id="6PDUWJHl8uD" role="KENke">
      <property role="TrG5h" value="invoer" />
      <ref role="KGglo" node="6PDUWJHkXWs" resolve="Invoer_Aanvraag_IIT" />
    </node>
    <node concept="KBdxu" id="6PDUWJHl8uL" role="KENnS">
      <property role="TrG5h" value="besluit" />
      <ref role="KGglo" node="6PDUWJHkXXB" resolve="Uitvoer_Besluit_IIT" />
    </node>
    <node concept="3AW6rv" id="6PDUWJHl9sh" role="21XpMX">
      <node concept="1EDDcM" id="6PDUWJHl9st" role="3AW66m" />
      <node concept="2R$z7" id="6PDUWJHl9sp" role="2KWIQ6">
        <property role="2RIz2" value="30CduGMXDSJ/boolean" />
      </node>
    </node>
    <node concept="3AW6rv" id="6PDUWJHlbon" role="21XpMX">
      <node concept="THod0" id="6PDUWJHlboG" role="3AW66m" />
      <node concept="2R$z7" id="6PDUWJHlboC" role="2KWIQ6">
        <property role="2RIz2" value="30CduGMXDbm/string" />
      </node>
    </node>
    <node concept="3AW6rv" id="3l119$$Vbqu" role="21XpMX">
      <node concept="1EDDfm" id="3l119$$Vbuo" role="3AW66m">
        <ref role="1EDDfl" to="6mfl:3l119$$V7zv" resolve="bedrag" />
      </node>
      <node concept="3x25J3" id="3l119$$VbrV" role="2KWIQ6">
        <ref role="3x24DM" node="3l119$$Vbbk" resolve="bedrag" />
      </node>
    </node>
    <node concept="2nR7yY" id="6PDUWJHlrFO" role="2nR7zj">
      <ref role="2nR7yZ" to="35ve:6PDUWJHlm9k" resolve="parameterset 2021" />
    </node>
    <node concept="2OB35" id="3l119$$Vbbk" role="2P2lV">
      <property role="TrG5h" value="bedrag" />
      <node concept="2R$z7" id="3l119$$Vbf$" role="2Evv_c">
        <property role="2RIz2" value="30CduGMXE5a/decimal" />
      </node>
      <node concept="3ytzF" id="3l119$$VbhY" role="2OxYR">
        <property role="3ytzC" value="0" />
        <property role="3ytzJ" value="2" />
      </node>
    </node>
  </node>
  <node concept="2P7X8V" id="6PDUWJHljzc">
    <property role="3GE5qa" value="xsd" />
    <property role="2P7b_s" value="0.1" />
    <property role="2P7ezu" value="15 feb. 2021 15:18:19" />
    <property role="2P4Thn" value="false" />
    <ref role="2P77Mq" node="6PDUWJHl8uC" resolve="IIT" />
    <node concept="1yAGSL" id="6PDUWJHljzd" role="2ON9hV">
      <property role="TrG5h" value="iit" />
      <property role="3GE5qa" value="rsiit" />
      <node concept="3rIKKV" id="6PDUWJHljze" role="2pMbU3">
        <node concept="2pNm8N" id="6PDUWJHljzf" role="2pNm8Q">
          <node concept="2e3yu2" id="6PDUWJHljzg" role="BGLLu">
            <property role="1D$jbd" value="1.0" />
          </node>
        </node>
        <node concept="2pNNFK" id="6PDUWJHljzh" role="2pNm8H">
          <property role="2pNNFO" value="xsd:schema" />
          <node concept="2pNNFK" id="6PDUWJHljzi" role="3o6s8t">
            <property role="2pNNFO" value="xsd:element" />
            <property role="qg3DV" value="true" />
            <node concept="2pNUuL" id="6PDUWJHljzj" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHljzk" role="2pMdts">
                <property role="2pMdty" value="rsiitMsg" />
              </node>
            </node>
            <node concept="1JL9iB" id="6PDUWJHljzl" role="2pNNFR">
              <property role="1JMLRT" value="type" />
              <ref role="1JL9iA" node="6PDUWJHljzu" resolve="Message" />
            </node>
          </node>
          <node concept="2pNUuL" id="6PDUWJHljzm" role="2pNNFR">
            <property role="2pNUuO" value="version" />
            <node concept="2pMdtt" id="6PDUWJHljzn" role="2pMdts">
              <property role="2pMdty" value="0.1" />
            </node>
          </node>
          <node concept="2pNUuL" id="6PDUWJHljzo" role="2pNNFR">
            <property role="2pNUuO" value="targetNamespace" />
            <node concept="2pMdtt" id="6PDUWJHljzp" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="6PDUWJHljzq" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:iit" />
            <node concept="2pMdtt" id="6PDUWJHljzr" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="6PDUWJHljzs" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:xsd" />
            <node concept="2pMdtt" id="6PDUWJHljzt" role="2pMdts">
              <property role="2pMdty" value="http://www.w3.org/2001/XMLSchema" />
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHljzu" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHljzv" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHljzw" role="2pMdts">
                <property role="2pMdty" value="Message" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHljzx" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="6PDUWJHljzy" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHljzz" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHljz$" role="2pMdts">
                    <property role="2pMdty" value="request" />
                  </node>
                </node>
                <node concept="1JL9iB" id="6PDUWJHljz_" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="6PDUWJHljzG" resolve="Request" />
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHljzA" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHljzB" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHljzC" role="2pMdts">
                    <property role="2pMdty" value="response" />
                  </node>
                </node>
                <node concept="1JL9iB" id="6PDUWJHljzD" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="6PDUWJHlj$1" resolve="Response" />
                </node>
                <node concept="2pNUuL" id="6PDUWJHljzE" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="6PDUWJHljzF" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHljzG" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHljzH" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHljzI" role="2pMdts">
                <property role="2pMdty" value="Request" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHljzJ" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="6PDUWJHljzK" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHljzL" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHljzM" role="2pMdts">
                    <property role="2pMdty" value="invoer" />
                  </node>
                </node>
                <node concept="1JL9iB" id="6PDUWJHljzN" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="6PDUWJHlj$B" resolve="Invoer__aanvraag__iit" />
                </node>
                <node concept="2pNUuL" id="6PDUWJHljzO" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="6PDUWJHljzP" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1JMoa7" id="6PDUWJHljzQ" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="6PDUWJHljzR" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="6PDUWJHljzS" role="2pMdts">
                  <property role="2pMdty" value="belastingjaar" />
                </node>
              </node>
              <node concept="1JL9iB" id="6PDUWJHljzT" role="2pNNFR">
                <property role="1JMLRT" value="type" />
                <ref role="1JL9iA" node="6PDUWJHlj_w" resolve="belastingjaarType" />
              </node>
              <node concept="2pNUuL" id="6PDUWJHljzU" role="2pNNFR">
                <property role="2pNUuO" value="use" />
                <node concept="2pMdtt" id="6PDUWJHljzV" role="2pMdts">
                  <property role="2pMdty" value="required" />
                </node>
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHljzW" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="6PDUWJHljzX" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="6PDUWJHljzY" role="2pMdts">
                  <property role="2pMdty" value="berichtId" />
                </node>
              </node>
              <node concept="2pNUuL" id="6PDUWJHljzZ" role="2pNNFR">
                <property role="2pNUuO" value="type" />
                <node concept="2pMdtt" id="6PDUWJHlj$0" role="2pMdts">
                  <property role="2pMdty" value="xsd:string" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHlj$1" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHlj$2" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHlj$3" role="2pMdts">
                <property role="2pMdty" value="Response" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHlj$4" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="6PDUWJHlj$5" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$6" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$7" role="2pMdts">
                    <property role="2pMdty" value="serviceResultaat" />
                  </node>
                </node>
                <node concept="1JL9iB" id="6PDUWJHlj$8" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="6PDUWJHlj$f" resolve="ServiceResultaat" />
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$a" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$b" role="2pMdts">
                    <property role="2pMdty" value="besluit" />
                  </node>
                </node>
                <node concept="1JL9iB" id="6PDUWJHlj$c" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="6PDUWJHlj_e" resolve="Uitvoer__besluit__iit" />
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$d" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="6PDUWJHlj$e" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHlj$f" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHlj$g" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHlj$h" role="2pMdts">
                <property role="2pMdty" value="ServiceResultaat" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHlj$i" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="6PDUWJHlj$j" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$k" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$l" role="2pMdts">
                    <property role="2pMdty" value="resultaatcode" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$m" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$n" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$o" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$p" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$q" role="2pMdts">
                    <property role="2pMdty" value="resultaatmelding" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$r" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$s" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$t" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$u" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$v" role="2pMdts">
                    <property role="2pMdty" value="rulesversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$w" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$x" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$y" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$z" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$$" role="2pMdts">
                    <property role="2pMdty" value="serviceversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$_" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$A" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHlj$B" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHlj$C" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHlj$D" role="2pMdts">
                <property role="2pMdty" value="Invoer__aanvraag__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHlj$E" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="6PDUWJHlj$F" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$G" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$H" role="2pMdts">
                    <property role="2pMdty" value="woonplaats" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$I" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$J" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$K" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$L" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$M" role="2pMdts">
                    <property role="2pMdty" value="aowLeeftijdBehaald" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$N" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$O" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$P" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$Q" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$R" role="2pMdts">
                    <property role="2pMdty" value="ouderDan21" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$S" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$T" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$U" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj$V" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj$W" role="2pMdts">
                    <property role="2pMdty" value="alleenstaand" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj$X" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj$Y" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj$Z" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj_0" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj_1" role="2pMdts">
                    <property role="2pMdty" value="thuiswonendeKinderen" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_2" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj_3" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj_4" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj_5" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj_6" role="2pMdts">
                    <property role="2pMdty" value="inkomenPerMaand" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_7" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj_8" role="2pMdts">
                    <property role="2pMdty" value="xsd:double" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj_9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj_a" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj_b" role="2pMdts">
                    <property role="2pMdty" value="vermogen" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_c" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj_d" role="2pMdts">
                    <property role="2pMdty" value="xsd:integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHlj_e" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="6PDUWJHlj_f" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHlj_g" role="2pMdts">
                <property role="2pMdty" value="Uitvoer__besluit__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHlj_h" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="6PDUWJHlj_i" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj_j" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj_k" role="2pMdts">
                    <property role="2pMdty" value="rechtBeschrijving" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_l" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj_m" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_n" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="6PDUWJHlj_o" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="6PDUWJHlj_p" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj_q" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="6PDUWJHlj_r" role="2pMdts">
                    <property role="2pMdty" value="uitTeKerenToeslagBedrag" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_s" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="6PDUWJHlj_t" role="2pMdts">
                    <property role="2pMdty" value="xsd:integer" />
                  </node>
                </node>
                <node concept="2pNUuL" id="6PDUWJHlj_u" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="6PDUWJHlj_v" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="6PDUWJHlj_w" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="6PDUWJHlj_x" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="6PDUWJHlj_y" role="2pMdts">
                <property role="2pMdty" value="belastingjaarType" />
              </node>
            </node>
            <node concept="2pNNFK" id="6PDUWJHlj_z" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="6PDUWJHlj_$" role="3o6s8t">
                <property role="2pNNFO" value="xsd:totalDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6PDUWJHlj__" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6PDUWJHlj_A" role="2pMdts">
                    <property role="2pMdty" value="4" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="6PDUWJHlj_B" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="6PDUWJHlj_C" role="2pMdts">
                  <property role="2pMdty" value="xsd:short" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2P7X8V" id="3l119$$VbPj">
    <property role="3GE5qa" value="xsd" />
    <property role="2P7b_s" value="0.2" />
    <property role="2P7ezu" value="16 feb. 2021 09:15:20" />
    <property role="2P4Thn" value="false" />
    <ref role="2P77Mq" node="6PDUWJHl8uC" resolve="IIT" />
    <node concept="1yAGSL" id="3l119$$VbPk" role="2ON9hV">
      <property role="TrG5h" value="iit" />
      <property role="3GE5qa" value="rsiit" />
      <node concept="3rIKKV" id="3l119$$VbPl" role="2pMbU3">
        <node concept="2pNm8N" id="3l119$$VbPm" role="2pNm8Q">
          <node concept="2e3yu2" id="3l119$$VbPn" role="BGLLu">
            <property role="1D$jbd" value="1.0" />
          </node>
        </node>
        <node concept="2pNNFK" id="3l119$$VbPo" role="2pNm8H">
          <property role="2pNNFO" value="xsd:schema" />
          <node concept="2pNNFK" id="3l119$$VbPp" role="3o6s8t">
            <property role="2pNNFO" value="xsd:element" />
            <property role="qg3DV" value="true" />
            <node concept="2pNUuL" id="3l119$$VbPq" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbPr" role="2pMdts">
                <property role="2pMdty" value="rsiitMsg" />
              </node>
            </node>
            <node concept="1JL9iB" id="3l119$$VbPs" role="2pNNFR">
              <property role="1JMLRT" value="type" />
              <ref role="1JL9iA" node="3l119$$VbP_" resolve="Message" />
            </node>
          </node>
          <node concept="2pNUuL" id="3l119$$VbPt" role="2pNNFR">
            <property role="2pNUuO" value="version" />
            <node concept="2pMdtt" id="3l119$$VbPu" role="2pMdts">
              <property role="2pMdty" value="0.2" />
            </node>
          </node>
          <node concept="2pNUuL" id="3l119$$VbPv" role="2pNNFR">
            <property role="2pNUuO" value="targetNamespace" />
            <node concept="2pMdtt" id="3l119$$VbPw" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="3l119$$VbPx" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:iit" />
            <node concept="2pMdtt" id="3l119$$VbPy" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="3l119$$VbPz" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:xsd" />
            <node concept="2pMdtt" id="3l119$$VbP$" role="2pMdts">
              <property role="2pMdty" value="http://www.w3.org/2001/XMLSchema" />
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbP_" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbPA" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbPB" role="2pMdts">
                <property role="2pMdty" value="Message" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbPC" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="3l119$$VbPD" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbPE" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbPF" role="2pMdts">
                    <property role="2pMdty" value="request" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbPG" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbPN" resolve="Request" />
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbPH" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbPI" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbPJ" role="2pMdts">
                    <property role="2pMdty" value="response" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbPK" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbQ8" resolve="Response" />
                </node>
                <node concept="2pNUuL" id="3l119$$VbPL" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="3l119$$VbPM" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbPN" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbPO" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbPP" role="2pMdts">
                <property role="2pMdty" value="Request" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbPQ" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="3l119$$VbPR" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbPS" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbPT" role="2pMdts">
                    <property role="2pMdty" value="invoer" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbPU" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbQI" resolve="Invoer__aanvraag__iit" />
                </node>
                <node concept="2pNUuL" id="3l119$$VbPV" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="3l119$$VbPW" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1JMoa7" id="3l119$$VbPX" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="3l119$$VbPY" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="3l119$$VbPZ" role="2pMdts">
                  <property role="2pMdty" value="belastingjaar" />
                </node>
              </node>
              <node concept="1JL9iB" id="3l119$$VbQ0" role="2pNNFR">
                <property role="1JMLRT" value="type" />
                <ref role="1JL9iA" node="3l119$$VbRH" resolve="belastingjaarType" />
              </node>
              <node concept="2pNUuL" id="3l119$$VbQ1" role="2pNNFR">
                <property role="2pNUuO" value="use" />
                <node concept="2pMdtt" id="3l119$$VbQ2" role="2pMdts">
                  <property role="2pMdty" value="required" />
                </node>
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbQ3" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="3l119$$VbQ4" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="3l119$$VbQ5" role="2pMdts">
                  <property role="2pMdty" value="berichtId" />
                </node>
              </node>
              <node concept="2pNUuL" id="3l119$$VbQ6" role="2pNNFR">
                <property role="2pNUuO" value="type" />
                <node concept="2pMdtt" id="3l119$$VbQ7" role="2pMdts">
                  <property role="2pMdty" value="xsd:string" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbQ8" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbQ9" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbQa" role="2pMdts">
                <property role="2pMdty" value="Response" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbQb" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="3l119$$VbQc" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQd" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQe" role="2pMdts">
                    <property role="2pMdty" value="serviceResultaat" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbQf" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbQm" resolve="ServiceResultaat" />
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQg" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQh" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQi" role="2pMdts">
                    <property role="2pMdty" value="besluit" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbQj" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbRj" resolve="Uitvoer__besluit__iit" />
                </node>
                <node concept="2pNUuL" id="3l119$$VbQk" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="3l119$$VbQl" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbQm" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbQn" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbQo" role="2pMdts">
                <property role="2pMdty" value="ServiceResultaat" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbQp" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="3l119$$VbQq" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQr" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQs" role="2pMdts">
                    <property role="2pMdty" value="resultaatcode" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQt" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQu" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQv" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQw" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQx" role="2pMdts">
                    <property role="2pMdty" value="resultaatmelding" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQy" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQz" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQ$" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQ_" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQA" role="2pMdts">
                    <property role="2pMdty" value="rulesversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQB" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQC" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQD" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQE" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQF" role="2pMdts">
                    <property role="2pMdty" value="serviceversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQG" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQH" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbQI" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbQJ" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbQK" role="2pMdts">
                <property role="2pMdty" value="Invoer__aanvraag__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbQL" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="3l119$$VbQM" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQN" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQO" role="2pMdts">
                    <property role="2pMdty" value="woonplaats" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQP" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQQ" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQR" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQS" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQT" role="2pMdts">
                    <property role="2pMdty" value="aowLeeftijdBehaald" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQU" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbQV" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbQW" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbQX" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbQY" role="2pMdts">
                    <property role="2pMdty" value="ouderDan21" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbQZ" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbR0" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbR1" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbR2" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbR3" role="2pMdts">
                    <property role="2pMdty" value="alleenstaand" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbR4" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbR5" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbR6" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbR7" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbR8" role="2pMdts">
                    <property role="2pMdty" value="thuiswonendeKinderen" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbR9" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbRa" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbRb" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRc" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbRd" role="2pMdts">
                    <property role="2pMdty" value="inkomenPerMaand" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbRe" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbR$" resolve="bedrag" />
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbRf" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRg" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbRh" role="2pMdts">
                    <property role="2pMdty" value="vermogen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbRi" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbR$" resolve="bedrag" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbRj" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="3l119$$VbRk" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbRl" role="2pMdts">
                <property role="2pMdty" value="Uitvoer__besluit__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbRm" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="3l119$$VbRn" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRo" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbRp" role="2pMdts">
                    <property role="2pMdty" value="rechtBeschrijving" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbRq" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="3l119$$VbRr" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
                <node concept="2pNUuL" id="3l119$$VbRs" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="3l119$$VbRt" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="3l119$$VbRu" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRv" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="3l119$$VbRw" role="2pMdts">
                    <property role="2pMdty" value="uitTeKerenToeslagBedrag" />
                  </node>
                </node>
                <node concept="1JL9iB" id="3l119$$VbRx" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="3l119$$VbR$" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="3l119$$VbRy" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="3l119$$VbRz" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbR$" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="3l119$$VbR_" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbRA" role="2pMdts">
                <property role="2pMdty" value="bedrag" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbRB" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="3l119$$VbRC" role="3o6s8t">
                <property role="2pNNFO" value="xsd:fractionDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRD" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="3l119$$VbRE" role="2pMdts">
                    <property role="2pMdty" value="2" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="3l119$$VbRF" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="3l119$$VbRG" role="2pMdts">
                  <property role="2pMdty" value="xsd:decimal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="3l119$$VbRH" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="3l119$$VbRI" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="3l119$$VbRJ" role="2pMdts">
                <property role="2pMdty" value="belastingjaarType" />
              </node>
            </node>
            <node concept="2pNNFK" id="3l119$$VbRK" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="3l119$$VbRL" role="3o6s8t">
                <property role="2pNNFO" value="xsd:totalDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="3l119$$VbRM" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="3l119$$VbRN" role="2pMdts">
                    <property role="2pMdty" value="4" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="3l119$$VbRO" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="3l119$$VbRP" role="2pMdts">
                  <property role="2pMdty" value="xsd:short" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2kTx5H" id="7J1k8s1Fzgx">
    <property role="OXFlC" value="Message" />
    <property role="OWdfG" value="Request" />
    <property role="OWdfy" value="Response" />
    <property role="OXSx3" value="belastingjaar" />
    <property role="TrG5h" value="BSC" />
    <property role="2R2JXj" value="vil" />
    <property role="2R2JWx" value="bsc" />
    <property role="3GE5qa" value="Bijdrage Sport en Cultuur" />
    <property role="2QnZO9" value="0.2" />
    <ref role="2kTx5C" to="xshh:1qYcgYdZkgo" resolve="Bepalen bijdrage voor sport en cultuur" />
    <node concept="3AW6rv" id="7J1k8s1FND0" role="21XpMX">
      <node concept="1EDDcM" id="7J1k8s1FND1" role="3AW66m" />
      <node concept="2R$z7" id="7J1k8s1FND2" role="2KWIQ6">
        <property role="2RIz2" value="30CduGMXDSJ/boolean" />
      </node>
    </node>
    <node concept="3AW6rv" id="7J1k8s1FND3" role="21XpMX">
      <node concept="THod0" id="7J1k8s1FND4" role="3AW66m" />
      <node concept="2R$z7" id="7J1k8s1FND5" role="2KWIQ6">
        <property role="2RIz2" value="30CduGMXDbm/string" />
      </node>
    </node>
    <node concept="3AW6rv" id="7J1k8s1FND6" role="21XpMX">
      <node concept="1EDDfm" id="7J1k8s1FND7" role="3AW66m">
        <ref role="1EDDfl" to="6mfl:3l119$$V7zv" resolve="bedrag" />
      </node>
      <node concept="3x25J3" id="7J1k8s1FND8" role="2KWIQ6">
        <ref role="3x24DM" node="7J1k8s1FNlK" resolve="bedrag" />
      </node>
    </node>
    <node concept="2nR7yY" id="7J1k8s1FzsA" role="2nR7zj">
      <ref role="2nR7yZ" to="35ve:6PDUWJHlm9k" resolve="parameterset 2021" />
    </node>
    <node concept="KB4bO" id="7J1k8s1FBD8" role="KENke">
      <property role="TrG5h" value="invoer" />
      <ref role="KGglo" node="7J1k8s1FzCw" resolve="Invoer_Aanvraag_BSC" />
    </node>
    <node concept="KBdxu" id="7J1k8s1FBJQ" role="KENnS">
      <property role="TrG5h" value="besluit" />
      <ref role="KGglo" node="7J1k8s1FADQ" resolve="Uitvoer_Aanvraag_BSC" />
    </node>
    <node concept="2OB35" id="7J1k8s1FNlK" role="2P2lV">
      <property role="TrG5h" value="bedrag" />
      <node concept="2R$z7" id="7J1k8s1FNpq" role="2Evv_c">
        <property role="2RIz2" value="30CduGMXE5a/decimal" />
      </node>
      <node concept="3ytzF" id="7J1k8s1FNxg" role="2OxYR">
        <property role="3ytzC" value="0" />
        <property role="3ytzJ" value="2" />
      </node>
    </node>
  </node>
  <node concept="2785BV" id="7J1k8s1FzCw">
    <property role="TrG5h" value="Invoer_Aanvraag_BSC" />
    <property role="3GE5qa" value="Bijdrage Sport en Cultuur" />
    <ref role="1Ig6_r" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1IH5HN" id="7J1k8s1F$zC" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_ix" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_mN" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
    </node>
    <node concept="1IH5HN" id="2475v2lZwz$" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_uh" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_wh" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_B3" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_G5" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
    </node>
    <node concept="1IH5HN" id="7J1k8s1F_Pj" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
    </node>
  </node>
  <node concept="2785BU" id="7J1k8s1FADQ">
    <property role="TrG5h" value="Uitvoer_Aanvraag_BSC" />
    <property role="3GE5qa" value="Bijdrage Sport en Cultuur" />
    <ref role="1Ig6_r" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
    <node concept="1IHXn0" id="7J1k8s1FBA8" role="2785Bw">
      <ref role="1IJyWM" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
    </node>
  </node>
  <node concept="2P7X8V" id="7J1k8s1FT9g">
    <property role="3GE5qa" value="xsd" />
    <property role="2P7b_s" value="0.1" />
    <property role="2P7ezu" value="18 mrt. 2021 16:12:44" />
    <property role="2P4Thn" value="false" />
    <ref role="2P77Mq" node="7J1k8s1Fzgx" resolve="BSC" />
    <node concept="1yAGSL" id="7J1k8s1FT9h" role="2ON9hV">
      <property role="TrG5h" value="bsc" />
      <property role="3GE5qa" value="rsbsc" />
      <node concept="3rIKKV" id="7J1k8s1FT9i" role="2pMbU3">
        <node concept="2pNm8N" id="7J1k8s1FT9j" role="2pNm8Q">
          <node concept="2e3yu2" id="7J1k8s1FT9k" role="BGLLu">
            <property role="1D$jbd" value="1.0" />
          </node>
        </node>
        <node concept="2pNNFK" id="7J1k8s1FT9l" role="2pNm8H">
          <property role="2pNNFO" value="xsd:schema" />
          <node concept="2pNNFK" id="7J1k8s1FT9m" role="3o6s8t">
            <property role="2pNNFO" value="xsd:element" />
            <property role="qg3DV" value="true" />
            <node concept="2pNUuL" id="7J1k8s1FT9n" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FT9o" role="2pMdts">
                <property role="2pMdty" value="rsbscMsg" />
              </node>
            </node>
            <node concept="1JL9iB" id="7J1k8s1FT9p" role="2pNNFR">
              <property role="1JMLRT" value="type" />
              <ref role="1JL9iA" node="7J1k8s1FT9y" resolve="Message" />
            </node>
          </node>
          <node concept="2pNUuL" id="7J1k8s1FT9q" role="2pNNFR">
            <property role="2pNUuO" value="version" />
            <node concept="2pMdtt" id="7J1k8s1FT9r" role="2pMdts">
              <property role="2pMdty" value="0.1" />
            </node>
          </node>
          <node concept="2pNUuL" id="7J1k8s1FT9s" role="2pNNFR">
            <property role="2pNUuO" value="targetNamespace" />
            <node concept="2pMdtt" id="7J1k8s1FT9t" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/BscBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="7J1k8s1FT9u" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:bsc" />
            <node concept="2pMdtt" id="7J1k8s1FT9v" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/BscBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="7J1k8s1FT9w" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:xsd" />
            <node concept="2pMdtt" id="7J1k8s1FT9x" role="2pMdts">
              <property role="2pMdty" value="http://www.w3.org/2001/XMLSchema" />
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FT9y" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FT9z" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FT9$" role="2pMdts">
                <property role="2pMdty" value="Message" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FT9_" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="7J1k8s1FT9A" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FT9B" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FT9C" role="2pMdts">
                    <property role="2pMdty" value="request" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FT9D" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FT9K" resolve="Request" />
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FT9E" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FT9F" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FT9G" role="2pMdts">
                    <property role="2pMdty" value="response" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FT9H" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTa5" resolve="Response" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FT9I" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FT9J" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FT9K" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FT9L" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FT9M" role="2pMdts">
                <property role="2pMdty" value="Request" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FT9N" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="7J1k8s1FT9O" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FT9P" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FT9Q" role="2pMdts">
                    <property role="2pMdty" value="invoer" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FT9R" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTaF" resolve="Invoer__aanvraag__bsc" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FT9S" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FT9T" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1JMoa7" id="7J1k8s1FT9U" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="7J1k8s1FT9V" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="7J1k8s1FT9W" role="2pMdts">
                  <property role="2pMdty" value="belastingjaar" />
                </node>
              </node>
              <node concept="1JL9iB" id="7J1k8s1FT9X" role="2pNNFR">
                <property role="1JMLRT" value="type" />
                <ref role="1JL9iA" node="7J1k8s1FTbZ" resolve="belastingjaarType" />
              </node>
              <node concept="2pNUuL" id="7J1k8s1FT9Y" role="2pNNFR">
                <property role="2pNUuO" value="use" />
                <node concept="2pMdtt" id="7J1k8s1FT9Z" role="2pMdts">
                  <property role="2pMdty" value="required" />
                </node>
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTa0" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="7J1k8s1FTa1" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="7J1k8s1FTa2" role="2pMdts">
                  <property role="2pMdty" value="berichtId" />
                </node>
              </node>
              <node concept="2pNUuL" id="7J1k8s1FTa3" role="2pNNFR">
                <property role="2pNUuO" value="type" />
                <node concept="2pMdtt" id="7J1k8s1FTa4" role="2pMdts">
                  <property role="2pMdty" value="xsd:string" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTa5" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FTa6" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTa7" role="2pMdts">
                <property role="2pMdty" value="Response" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTa8" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="7J1k8s1FTa9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTaa" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTab" role="2pMdts">
                    <property role="2pMdty" value="serviceResultaat" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FTac" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTaj" resolve="ServiceResultaat" />
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTad" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTae" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaf" role="2pMdts">
                    <property role="2pMdty" value="besluit" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FTag" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTbG" resolve="Uitvoer__aanvraag__bsc" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTah" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTai" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTaj" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FTak" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTal" role="2pMdts">
                <property role="2pMdty" value="ServiceResultaat" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTam" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="7J1k8s1FTan" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTao" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTap" role="2pMdts">
                    <property role="2pMdty" value="resultaatcode" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaq" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTar" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTas" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTat" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTau" role="2pMdts">
                    <property role="2pMdty" value="resultaatmelding" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTav" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTaw" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTax" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTay" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaz" role="2pMdts">
                    <property role="2pMdty" value="rulesversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTa$" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTa_" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTaA" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTaB" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaC" role="2pMdts">
                    <property role="2pMdty" value="serviceversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaD" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTaE" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTaF" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FTaG" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTaH" role="2pMdts">
                <property role="2pMdty" value="Invoer__aanvraag__bsc" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTaI" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="7J1k8s1FTaJ" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTaK" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaL" role="2pMdts">
                    <property role="2pMdty" value="woonplaats" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaM" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTaN" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaO" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTaP" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTaQ" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTaR" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaS" role="2pMdts">
                    <property role="2pMdty" value="vanaf18TotEnMet20Jaar" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaT" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTaU" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTaV" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTaW" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTaX" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTaY" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTaZ" role="2pMdts">
                    <property role="2pMdty" value="alleenstaandeOfAlleenstaandeOuder" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTb0" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTb1" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTb2" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTb3" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTb4" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTb5" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTb6" role="2pMdts">
                    <property role="2pMdty" value="thuiswonendeKinderen" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTb7" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTb8" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTb9" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTba" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTbb" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbc" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbd" role="2pMdts">
                    <property role="2pMdty" value="gezinsinkomen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FTbe" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTbQ" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbf" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTbg" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTbh" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbi" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbj" role="2pMdts">
                    <property role="2pMdty" value="gezamenlijkVermogen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FTbk" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTbQ" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbl" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTbm" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTbn" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbo" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbp" role="2pMdts">
                    <property role="2pMdty" value="gehuwdOfSamenwonend" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbq" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTbr" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbs" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTbt" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTbu" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbv" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbw" role="2pMdts">
                    <property role="2pMdty" value="partnerJongerDan21" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbx" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTby" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbz" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTb$" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="7J1k8s1FTb_" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbA" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbB" role="2pMdts">
                    <property role="2pMdty" value="vanaf21TotDeAowLeeftijd" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbC" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="7J1k8s1FTbD" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbE" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTbF" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTbG" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="7J1k8s1FTbH" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTbI" role="2pMdts">
                <property role="2pMdty" value="Uitvoer__aanvraag__bsc" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTbJ" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="7J1k8s1FTbK" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbL" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="7J1k8s1FTbM" role="2pMdts">
                    <property role="2pMdty" value="uitTeKerenBijdrageVoorSportEnCultuur" />
                  </node>
                </node>
                <node concept="1JL9iB" id="7J1k8s1FTbN" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="7J1k8s1FTbQ" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="7J1k8s1FTbO" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="7J1k8s1FTbP" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTbQ" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="7J1k8s1FTbR" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTbS" role="2pMdts">
                <property role="2pMdty" value="bedrag" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTbT" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="7J1k8s1FTbU" role="3o6s8t">
                <property role="2pNNFO" value="xsd:fractionDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTbV" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="7J1k8s1FTbW" role="2pMdts">
                    <property role="2pMdty" value="2" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="7J1k8s1FTbX" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="7J1k8s1FTbY" role="2pMdts">
                  <property role="2pMdty" value="xsd:decimal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="7J1k8s1FTbZ" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="7J1k8s1FTc0" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="7J1k8s1FTc1" role="2pMdts">
                <property role="2pMdty" value="belastingjaarType" />
              </node>
            </node>
            <node concept="2pNNFK" id="7J1k8s1FTc2" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="7J1k8s1FTc3" role="3o6s8t">
                <property role="2pNNFO" value="xsd:totalDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="7J1k8s1FTc4" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="7J1k8s1FTc5" role="2pMdts">
                    <property role="2pMdty" value="4" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="7J1k8s1FTc6" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="7J1k8s1FTc7" role="2pMdts">
                  <property role="2pMdty" value="xsd:short" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2P7X8V" id="1qYcgYdXQU_">
    <property role="3GE5qa" value="xsd" />
    <property role="2P7b_s" value="0.3" />
    <property role="2P7ezu" value="15 mrt. 2021 12:36:30" />
    <property role="2P4Thn" value="false" />
    <ref role="2P77Mq" node="6PDUWJHl8uC" resolve="IIT" />
    <node concept="1yAGSL" id="1qYcgYdXQUA" role="2ON9hV">
      <property role="TrG5h" value="iit" />
      <property role="3GE5qa" value="rsiit" />
      <node concept="3rIKKV" id="1qYcgYdXQUB" role="2pMbU3">
        <node concept="2pNm8N" id="1qYcgYdXQUC" role="2pNm8Q">
          <node concept="2e3yu2" id="1qYcgYdXQUD" role="BGLLu">
            <property role="1D$jbd" value="1.0" />
          </node>
        </node>
        <node concept="2pNNFK" id="1qYcgYdXQUE" role="2pNm8H">
          <property role="2pNNFO" value="xsd:schema" />
          <node concept="2pNNFK" id="1qYcgYdXQUF" role="3o6s8t">
            <property role="2pNNFO" value="xsd:element" />
            <property role="qg3DV" value="true" />
            <node concept="2pNUuL" id="1qYcgYdXQUG" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQUH" role="2pMdts">
                <property role="2pMdty" value="rsiitMsg" />
              </node>
            </node>
            <node concept="1JL9iB" id="1qYcgYdXQUI" role="2pNNFR">
              <property role="1JMLRT" value="type" />
              <ref role="1JL9iA" node="1qYcgYdXQUR" resolve="Message" />
            </node>
          </node>
          <node concept="2pNUuL" id="1qYcgYdXQUJ" role="2pNNFR">
            <property role="2pNUuO" value="version" />
            <node concept="2pMdtt" id="1qYcgYdXQUK" role="2pMdts">
              <property role="2pMdty" value="0.3" />
            </node>
          </node>
          <node concept="2pNUuL" id="1qYcgYdXQUL" role="2pNNFR">
            <property role="2pNUuO" value="targetNamespace" />
            <node concept="2pMdtt" id="1qYcgYdXQUM" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="1qYcgYdXQUN" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:iit" />
            <node concept="2pMdtt" id="1qYcgYdXQUO" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/IitBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="1qYcgYdXQUP" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:xsd" />
            <node concept="2pMdtt" id="1qYcgYdXQUQ" role="2pMdts">
              <property role="2pMdty" value="http://www.w3.org/2001/XMLSchema" />
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQUR" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQUS" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQUT" role="2pMdts">
                <property role="2pMdty" value="Message" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQUU" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="1qYcgYdXQUV" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQUW" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQUX" role="2pMdts">
                    <property role="2pMdty" value="request" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQUY" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQV5" resolve="Request" />
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQUZ" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQV0" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQV1" role="2pMdts">
                    <property role="2pMdty" value="response" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQV2" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQVq" resolve="Response" />
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQV3" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="1qYcgYdXQV4" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQV5" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQV6" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQV7" role="2pMdts">
                <property role="2pMdty" value="Request" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQV8" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="1qYcgYdXQV9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVa" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVb" role="2pMdts">
                    <property role="2pMdty" value="invoer" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQVc" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQW0" resolve="Invoer__aanvraag__iit" />
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVd" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="1qYcgYdXQVe" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1JMoa7" id="1qYcgYdXQVf" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="1qYcgYdXQVg" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="1qYcgYdXQVh" role="2pMdts">
                  <property role="2pMdty" value="belastingjaar" />
                </node>
              </node>
              <node concept="1JL9iB" id="1qYcgYdXQVi" role="2pNNFR">
                <property role="1JMLRT" value="type" />
                <ref role="1JL9iA" node="1qYcgYdXQWZ" resolve="belastingjaarType" />
              </node>
              <node concept="2pNUuL" id="1qYcgYdXQVj" role="2pNNFR">
                <property role="2pNUuO" value="use" />
                <node concept="2pMdtt" id="1qYcgYdXQVk" role="2pMdts">
                  <property role="2pMdty" value="required" />
                </node>
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQVl" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="1qYcgYdXQVm" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="1qYcgYdXQVn" role="2pMdts">
                  <property role="2pMdty" value="berichtId" />
                </node>
              </node>
              <node concept="2pNUuL" id="1qYcgYdXQVo" role="2pNNFR">
                <property role="2pNUuO" value="type" />
                <node concept="2pMdtt" id="1qYcgYdXQVp" role="2pMdts">
                  <property role="2pMdty" value="xsd:string" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQVq" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQVr" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQVs" role="2pMdts">
                <property role="2pMdty" value="Response" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQVt" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="1qYcgYdXQVu" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVv" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVw" role="2pMdts">
                    <property role="2pMdty" value="serviceResultaat" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQVx" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQVC" resolve="ServiceResultaat" />
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQVy" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVz" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQV$" role="2pMdts">
                    <property role="2pMdty" value="besluit" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQV_" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQW_" resolve="Uitvoer__besluit__iit" />
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVA" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="1qYcgYdXQVB" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQVC" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQVD" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQVE" role="2pMdts">
                <property role="2pMdty" value="ServiceResultaat" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQVF" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="1qYcgYdXQVG" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVH" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVI" role="2pMdts">
                    <property role="2pMdty" value="resultaatcode" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVJ" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQVK" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQVL" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVM" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVN" role="2pMdts">
                    <property role="2pMdty" value="resultaatmelding" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVO" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQVP" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQVQ" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVR" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVS" role="2pMdts">
                    <property role="2pMdty" value="rulesversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVT" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQVU" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQVV" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQVW" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQVX" role="2pMdts">
                    <property role="2pMdty" value="serviceversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQVY" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQVZ" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQW0" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQW1" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQW2" role="2pMdts">
                <property role="2pMdty" value="Invoer__aanvraag__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQW3" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="1qYcgYdXQW4" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQW5" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQW6" role="2pMdts">
                    <property role="2pMdty" value="woonplaats" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQW7" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQW8" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQW9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWa" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWb" role="2pMdts">
                    <property role="2pMdty" value="aowLeeftijdBehaald" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWc" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQWd" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWe" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWf" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWg" role="2pMdts">
                    <property role="2pMdty" value="ouderDan21" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWh" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQWi" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWj" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWk" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWl" role="2pMdts">
                    <property role="2pMdty" value="alleenstaande" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWm" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQWn" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWo" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWp" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWq" role="2pMdts">
                    <property role="2pMdty" value="thuiswonendeKinderen" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWr" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQWs" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWt" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWu" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWv" role="2pMdts">
                    <property role="2pMdty" value="inkomenPerMaand" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQWw" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQWQ" resolve="bedrag" />
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWx" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWy" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWz" role="2pMdts">
                    <property role="2pMdty" value="vermogen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQW$" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQWQ" resolve="bedrag" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQW_" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="1qYcgYdXQWA" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQWB" role="2pMdts">
                <property role="2pMdty" value="Uitvoer__besluit__iit" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQWC" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="1qYcgYdXQWD" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWE" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWF" role="2pMdts">
                    <property role="2pMdty" value="rechtBeschrijving" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWG" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="1qYcgYdXQWH" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWI" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="1qYcgYdXQWJ" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="1qYcgYdXQWK" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWL" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="1qYcgYdXQWM" role="2pMdts">
                    <property role="2pMdty" value="uitTeKerenIndividueleInkomenstoeslag" />
                  </node>
                </node>
                <node concept="1JL9iB" id="1qYcgYdXQWN" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="1qYcgYdXQWQ" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="1qYcgYdXQWO" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="1qYcgYdXQWP" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQWQ" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="1qYcgYdXQWR" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQWS" role="2pMdts">
                <property role="2pMdty" value="bedrag" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQWT" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="1qYcgYdXQWU" role="3o6s8t">
                <property role="2pNNFO" value="xsd:fractionDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQWV" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="1qYcgYdXQWW" role="2pMdts">
                    <property role="2pMdty" value="2" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="1qYcgYdXQWX" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="1qYcgYdXQWY" role="2pMdts">
                  <property role="2pMdty" value="xsd:decimal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="1qYcgYdXQWZ" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="1qYcgYdXQX0" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="1qYcgYdXQX1" role="2pMdts">
                <property role="2pMdty" value="belastingjaarType" />
              </node>
            </node>
            <node concept="2pNNFK" id="1qYcgYdXQX2" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="1qYcgYdXQX3" role="3o6s8t">
                <property role="2pNNFO" value="xsd:totalDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="1qYcgYdXQX4" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="1qYcgYdXQX5" role="2pMdts">
                    <property role="2pMdty" value="4" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="1qYcgYdXQX6" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="1qYcgYdXQX7" role="2pMdts">
                  <property role="2pMdty" value="xsd:short" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2P7X8V" id="2475v2lZBGX">
    <property role="3GE5qa" value="xsd" />
    <property role="2P7b_s" value="0.2" />
    <property role="2P7ezu" value="29 mrt. 2021 10:33:01" />
    <property role="2P4Thn" value="false" />
    <ref role="2P77Mq" node="7J1k8s1Fzgx" resolve="BSC" />
    <node concept="1yAGSL" id="2475v2lZBGY" role="2ON9hV">
      <property role="TrG5h" value="bsc" />
      <property role="3GE5qa" value="rsbsc" />
      <node concept="3rIKKV" id="2475v2lZBGZ" role="2pMbU3">
        <node concept="2pNm8N" id="2475v2lZBH0" role="2pNm8Q">
          <node concept="2e3yu2" id="2475v2lZBH1" role="BGLLu">
            <property role="1D$jbd" value="1.0" />
          </node>
        </node>
        <node concept="2pNNFK" id="2475v2lZBH2" role="2pNm8H">
          <property role="2pNNFO" value="xsd:schema" />
          <node concept="2pNNFK" id="2475v2lZBH3" role="3o6s8t">
            <property role="2pNNFO" value="xsd:element" />
            <property role="qg3DV" value="true" />
            <node concept="2pNUuL" id="2475v2lZBH4" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBH5" role="2pMdts">
                <property role="2pMdty" value="rsbscMsg" />
              </node>
            </node>
            <node concept="1JL9iB" id="2475v2lZBH6" role="2pNNFR">
              <property role="1JMLRT" value="type" />
              <ref role="1JL9iA" node="2475v2lZBHf" resolve="Message" />
            </node>
          </node>
          <node concept="2pNUuL" id="2475v2lZBH7" role="2pNNFR">
            <property role="2pNUuO" value="version" />
            <node concept="2pMdtt" id="2475v2lZBH8" role="2pMdts">
              <property role="2pMdty" value="0.2" />
            </node>
          </node>
          <node concept="2pNUuL" id="2475v2lZBH9" role="2pNNFR">
            <property role="2pNUuO" value="targetNamespace" />
            <node concept="2pMdtt" id="2475v2lZBHa" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/BscBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="2475v2lZBHb" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:bsc" />
            <node concept="2pMdtt" id="2475v2lZBHc" role="2pMdts">
              <property role="2pMdty" value="http://www.belastingdienst.nl/BscBlazeBOM" />
            </node>
          </node>
          <node concept="2pNUuL" id="2475v2lZBHd" role="2pNNFR">
            <property role="2pNUuO" value="xmlns:xsd" />
            <node concept="2pMdtt" id="2475v2lZBHe" role="2pMdts">
              <property role="2pMdty" value="http://www.w3.org/2001/XMLSchema" />
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBHf" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBHg" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBHh" role="2pMdts">
                <property role="2pMdty" value="Message" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBHi" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="2475v2lZBHj" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBHk" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBHl" role="2pMdts">
                    <property role="2pMdty" value="request" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBHm" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBHt" resolve="Request" />
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBHn" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBHo" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBHp" role="2pMdts">
                    <property role="2pMdty" value="response" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBHq" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBHM" resolve="Response" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBHr" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBHs" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBHt" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBHu" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBHv" role="2pMdts">
                <property role="2pMdty" value="Request" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBHw" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="2475v2lZBHx" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBHy" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBHz" role="2pMdts">
                    <property role="2pMdty" value="invoer" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBH$" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBIo" resolve="Invoer__aanvraag__bsc" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBH_" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBHA" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1JMoa7" id="2475v2lZBHB" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="2475v2lZBHC" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="2475v2lZBHD" role="2pMdts">
                  <property role="2pMdty" value="belastingjaar" />
                </node>
              </node>
              <node concept="1JL9iB" id="2475v2lZBHE" role="2pNNFR">
                <property role="1JMLRT" value="type" />
                <ref role="1JL9iA" node="2475v2lZBJG" resolve="belastingjaarType" />
              </node>
              <node concept="2pNUuL" id="2475v2lZBHF" role="2pNNFR">
                <property role="2pNUuO" value="use" />
                <node concept="2pMdtt" id="2475v2lZBHG" role="2pMdts">
                  <property role="2pMdty" value="required" />
                </node>
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBHH" role="3o6s8t">
              <property role="2pNNFO" value="xsd:attribute" />
              <property role="qg3DV" value="true" />
              <node concept="2pNUuL" id="2475v2lZBHI" role="2pNNFR">
                <property role="2pNUuO" value="name" />
                <node concept="2pMdtt" id="2475v2lZBHJ" role="2pMdts">
                  <property role="2pMdty" value="berichtId" />
                </node>
              </node>
              <node concept="2pNUuL" id="2475v2lZBHK" role="2pNNFR">
                <property role="2pNUuO" value="type" />
                <node concept="2pMdtt" id="2475v2lZBHL" role="2pMdts">
                  <property role="2pMdty" value="xsd:string" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBHM" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBHN" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBHO" role="2pMdts">
                <property role="2pMdty" value="Response" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBHP" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="2475v2lZBHQ" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBHR" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBHS" role="2pMdts">
                    <property role="2pMdty" value="serviceResultaat" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBHT" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBI0" resolve="ServiceResultaat" />
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBHU" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBHV" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBHW" role="2pMdts">
                    <property role="2pMdty" value="besluit" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBHX" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBJp" resolve="Uitvoer__aanvraag__bsc" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBHY" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBHZ" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBI0" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBI1" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBI2" role="2pMdts">
                <property role="2pMdty" value="ServiceResultaat" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBI3" role="3o6s8t">
              <property role="2pNNFO" value="xsd:sequence" />
              <node concept="1JMoa7" id="2475v2lZBI4" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBI5" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBI6" role="2pMdts">
                    <property role="2pMdty" value="resultaatcode" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBI7" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBI8" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBI9" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIa" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIb" role="2pMdts">
                    <property role="2pMdty" value="resultaatmelding" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIc" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBId" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIe" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIf" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIg" role="2pMdts">
                    <property role="2pMdty" value="rulesversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIh" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBIi" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIj" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIk" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIl" role="2pMdts">
                    <property role="2pMdty" value="serviceversie" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIm" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBIn" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBIo" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBIp" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBIq" role="2pMdts">
                <property role="2pMdty" value="Invoer__aanvraag__bsc" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBIr" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="2475v2lZBIs" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIt" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIu" role="2pMdts">
                    <property role="2pMdty" value="woonplaats" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIv" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBIw" role="2pMdts">
                    <property role="2pMdty" value="xsd:string" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIx" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBIy" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIz" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBI$" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBI_" role="2pMdts">
                    <property role="2pMdty" value="vanaf18TotEnMet20Jaar" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIA" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBIB" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIC" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBID" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIE" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIF" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIG" role="2pMdts">
                    <property role="2pMdty" value="alleenstaandeOfAlleenstaandeOuder" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIH" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBII" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIJ" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBIK" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIL" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIM" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIN" role="2pMdts">
                    <property role="2pMdty" value="thuiswonendeKinderen0TM17Jaar" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIO" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBIP" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBIQ" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBIR" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIS" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIT" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBIU" role="2pMdts">
                    <property role="2pMdty" value="gezinsinkomen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBIV" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBJz" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBIW" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBIX" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBIY" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBIZ" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBJ0" role="2pMdts">
                    <property role="2pMdty" value="gezamenlijkVermogen" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBJ1" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBJz" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBJ2" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBJ3" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBJ4" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJ5" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBJ6" role="2pMdts">
                    <property role="2pMdty" value="gehuwdOfSamenwonend" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJ7" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBJ8" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJ9" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBJa" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBJb" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJc" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBJd" role="2pMdts">
                    <property role="2pMdty" value="partnerJongerDan21" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJe" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBJf" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJg" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBJh" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1JMoa7" id="2475v2lZBJi" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJj" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBJk" role="2pMdts">
                    <property role="2pMdty" value="vanaf21TotDeAowLeeftijd" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJl" role="2pNNFR">
                  <property role="2pNUuO" value="type" />
                  <node concept="2pMdtt" id="2475v2lZBJm" role="2pMdts">
                    <property role="2pMdty" value="xsd:boolean" />
                  </node>
                </node>
                <node concept="2pNUuL" id="2475v2lZBJn" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBJo" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBJp" role="3o6s8t">
            <property role="2pNNFO" value="xsd:complexType" />
            <node concept="2pNUuL" id="2475v2lZBJq" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBJr" role="2pMdts">
                <property role="2pMdty" value="Uitvoer__aanvraag__bsc" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBJs" role="3o6s8t">
              <property role="2pNNFO" value="xsd:all" />
              <node concept="1JMoa7" id="2475v2lZBJt" role="3o6s8t">
                <property role="2pNNFO" value="xsd:element" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJu" role="2pNNFR">
                  <property role="2pNUuO" value="name" />
                  <node concept="2pMdtt" id="2475v2lZBJv" role="2pMdts">
                    <property role="2pMdty" value="uitTeKerenBijdrageVoorSportEnCultuur" />
                  </node>
                </node>
                <node concept="1JL9iB" id="2475v2lZBJw" role="2pNNFR">
                  <property role="1JMLRT" value="type" />
                  <ref role="1JL9iA" node="2475v2lZBJz" resolve="bedrag" />
                </node>
                <node concept="2pNUuL" id="2475v2lZBJx" role="2pNNFR">
                  <property role="2pNUuO" value="minOccurs" />
                  <node concept="2pMdtt" id="2475v2lZBJy" role="2pMdts">
                    <property role="2pMdty" value="0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBJz" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="2475v2lZBJ$" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBJ_" role="2pMdts">
                <property role="2pMdty" value="bedrag" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBJA" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="2475v2lZBJB" role="3o6s8t">
                <property role="2pNNFO" value="xsd:fractionDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJC" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="2475v2lZBJD" role="2pMdts">
                    <property role="2pMdty" value="2" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="2475v2lZBJE" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="2475v2lZBJF" role="2pMdts">
                  <property role="2pMdty" value="xsd:decimal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1JMoa7" id="2475v2lZBJG" role="3o6s8t">
            <property role="2pNNFO" value="xsd:simpleType" />
            <node concept="2pNUuL" id="2475v2lZBJH" role="2pNNFR">
              <property role="2pNUuO" value="name" />
              <node concept="2pMdtt" id="2475v2lZBJI" role="2pMdts">
                <property role="2pMdty" value="belastingjaarType" />
              </node>
            </node>
            <node concept="2pNNFK" id="2475v2lZBJJ" role="3o6s8t">
              <property role="2pNNFO" value="xsd:restriction" />
              <node concept="2pNNFK" id="2475v2lZBJK" role="3o6s8t">
                <property role="2pNNFO" value="xsd:totalDigits" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="2475v2lZBJL" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="2475v2lZBJM" role="2pMdts">
                    <property role="2pMdty" value="4" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="2475v2lZBJN" role="2pNNFR">
                <property role="2pNUuO" value="base" />
                <node concept="2pMdtt" id="2475v2lZBJO" role="2pMdts">
                  <property role="2pMdty" value="xsd:short" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

