<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:80e97fbb-2f18-4780-a805-2d36e0c7decf(Rapportages)">
  <persistence version="9" />
  <languages>
    <use id="b2fc4154-1657-4d74-8828-c55b57a96ecd" name="rapporten" version="0" />
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="4w08" ref="r:d4c0de40-e3df-4f3d-b55d-4f7e70dbeb0b(Services)" />
  </imports>
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="b2fc4154-1657-4d74-8828-c55b57a96ecd" name="rapporten">
      <concept id="172689781860533492" name="rapporten.structure.EnkeleRoot" flags="ng" index="ihC25">
        <reference id="172689781860534369" name="root" index="ihCKg" />
      </concept>
      <concept id="8610067412508977244" name="rapporten.structure.Rapportage" flags="ng" index="3TJFbH">
        <child id="8610067412509012738" name="content" index="3TIiAN" />
      </concept>
    </language>
  </registry>
  <node concept="3TJFbH" id="3l119$$W8eg">
    <property role="TrG5h" value="Rapportage Individuele Inkomenstoeslag" />
    <node concept="ihC25" id="7J1k8s1Gmkt" role="3TIiAN">
      <ref role="ihCKg" to="4w08:6PDUWJHl8uC" resolve="IIT" />
    </node>
  </node>
  <node concept="3TJFbH" id="7J1k8s1GlPh">
    <property role="TrG5h" value="Rapportage Bijdrage Sport en Cultuur" />
    <node concept="ihC25" id="7J1k8s1Gm8H" role="3TIiAN">
      <ref role="ihCKg" to="4w08:7J1k8s1Fzgx" resolve="BSC" />
    </node>
  </node>
</model>

