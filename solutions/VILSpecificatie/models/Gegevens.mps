<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Gegevens)">
  <persistence version="9" />
  <languages>
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports />
  <registry>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="653687101152590770" name="gegevensspraak.structure.Kenmerk" flags="ng" index="2bpyt6">
        <property id="2589799484845947556" name="bezittelijk" index="3uiUDc" />
      </concept>
      <concept id="653687101152179938" name="gegevensspraak.structure.ObjectModel" flags="ng" index="2bv6Cm">
        <child id="653687101152179939" name="elem" index="2bv6Cn" unordered="true" />
      </concept>
      <concept id="653687101152178966" name="gegevensspraak.structure.Domein" flags="ng" index="2bv6Zy">
        <child id="5917060184181531817" name="base" index="1ECJDa" />
      </concept>
      <concept id="653687101152178956" name="gegevensspraak.structure.Attribuut" flags="ng" index="2bv6ZS">
        <child id="5917060184181247471" name="type" index="1EDDcc" />
      </concept>
      <concept id="653687101152157008" name="gegevensspraak.structure.ObjectType" flags="ng" index="2bvS6$">
        <child id="653687101152189607" name="elem" index="2bv01j" unordered="true" />
      </concept>
      <concept id="2800963173597667853" name="gegevensspraak.structure.Parameter" flags="ng" index="2DSAsB">
        <child id="5917060184181634509" name="type" index="1ERmGI" />
      </concept>
      <concept id="8878823228840241647" name="gegevensspraak.structure.TekstType" flags="ng" index="THod0" />
      <concept id="8989128614612178023" name="gegevensspraak.structure.Naamwoord" flags="ng" index="16ZtyY">
        <property id="8989128614612178052" name="isOnzijdig" index="16Ztxt" />
      </concept>
      <concept id="552830129173627999" name="gegevensspraak.structure.Koptekst" flags="ng" index="39aKxd">
        <property id="552830129173628020" name="tekst" index="39aKxA" />
      </concept>
      <concept id="5917060184181247441" name="gegevensspraak.structure.BooleanType" flags="ng" index="1EDDcM" />
      <concept id="5917060184181247326" name="gegevensspraak.structure.NumeriekType" flags="ng" index="1EDDeX" />
      <concept id="5917060184181247285" name="gegevensspraak.structure.DomeinType" flags="ng" index="1EDDfm">
        <reference id="5917060184181247286" name="domein" index="1EDDfl" />
      </concept>
      <concept id="3257175120315973651" name="gegevensspraak.structure.AbstractNumeriekType" flags="ng" index="3GBOYg">
        <property id="3257175120318322318" name="decimalen" index="3GST$d" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2bv6Cm" id="6PDUWJHkGCD">
    <property role="TrG5h" value="Gegevensmodel" />
    <node concept="2bvS6$" id="6PDUWJHkGCG" role="2bv6Cn">
      <property role="TrG5h" value="Aanvraag" />
      <node concept="39aKxd" id="3vw$xkYGXYt" role="2bv01j">
        <property role="39aKxA" value="Alle gemeenten" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkGD7" role="2bv01j">
        <property role="TrG5h" value="Woonplaats" />
        <node concept="THod0" id="6PDUWJHkGDY" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkGEB" role="2bv01j">
        <property role="TrG5h" value="AOW leeftijd behaald" />
        <node concept="1EDDcM" id="6PDUWJHkGHh" role="1EDDcc" />
      </node>
      <node concept="39aKxd" id="3vw$xkYGWFn" role="2bv01j">
        <property role="39aKxA" value="Utrecht" />
      </node>
      <node concept="2bpyt6" id="6PDUWJHkIiI" role="2bv01j">
        <property role="3uiUDc" value="true" />
        <property role="TrG5h" value="Aanvrager die woonachtig is in Utrecht" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkGR_" role="2bv01j">
        <property role="TrG5h" value="Thuiswonende kinderen" />
        <node concept="1EDDcM" id="6PDUWJHkGZO" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkGM3" role="2bv01j">
        <property role="TrG5h" value="Alleenstaande" />
        <node concept="1EDDcM" id="6PDUWJHkGPU" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkH1P" role="2bv01j">
        <property role="TrG5h" value="Inkomen per maand" />
        <property role="16Ztxt" value="true" />
        <node concept="1EDDfm" id="3l119$$VaQ6" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkH8F" role="2bv01j">
        <property role="TrG5h" value="Vermogen" />
        <property role="16Ztxt" value="true" />
        <node concept="1EDDfm" id="3l119$$VaTA" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkGIg" role="2bv01j">
        <property role="TrG5h" value="Ouder dan 21" />
        <node concept="1EDDcM" id="6PDUWJHkGKI" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkHfj" role="2bv01j">
        <property role="TrG5h" value="Recht beschrijving" />
        <node concept="THod0" id="6PDUWJHkHnL" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="6PDUWJHkHxD" role="2bv01j">
        <property role="16Ztxt" value="true" />
        <property role="TrG5h" value="Uit te keren individuele inkomenstoeslag" />
        <node concept="1EDDfm" id="3l119$$VaWw" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
      <node concept="39aKxd" id="3vw$xkYGWrL" role="2bv01j">
        <property role="39aKxA" value="Amersfoort" />
      </node>
      <node concept="2bpyt6" id="3vw$xkYGW4k" role="2bv01j">
        <property role="TrG5h" value="Aanvrager die woonachtig is in Amersfoort" />
        <property role="3uiUDc" value="true" />
      </node>
      <node concept="2bpyt6" id="1qYcgYdYC1w" role="2bv01j">
        <property role="3uiUDc" value="true" />
        <property role="TrG5h" value="Aanvrager die alleenstaande is" />
      </node>
      <node concept="2bpyt6" id="1qYcgYdYCpS" role="2bv01j">
        <property role="3uiUDc" value="true" />
        <property role="TrG5h" value="Aanvrager die alleenstaande ouder is" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYGWNo" role="2bv01j">
        <property role="TrG5h" value="vanaf 18 tot en met 20 jaar" />
        <node concept="1EDDcM" id="3vw$xkYGWXr" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYH9dD" role="2bv01j">
        <property role="TrG5h" value="vanaf 21 tot de AOW-leeftijd" />
        <node concept="1EDDcM" id="3vw$xkYH9XK" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="1qYcgYdYqqV" role="2bv01j">
        <property role="TrG5h" value="Thuiswonende kinderen 0 t/m 17 jaar" />
        <node concept="1EDDcM" id="1qYcgYdYqqW" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYHbEc" role="2bv01j">
        <property role="TrG5h" value="Partner jonger dan 21" />
        <node concept="1EDDcM" id="3vw$xkYHbEd" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYHa3V" role="2bv01j">
        <property role="TrG5h" value="Alleenstaande of alleenstaande ouder" />
        <node concept="1EDDcM" id="3vw$xkYHaa4" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYHaS6" role="2bv01j">
        <property role="TrG5h" value="Gehuwd of Samenwonend" />
        <node concept="1EDDcM" id="3vw$xkYHb7m" role="1EDDcc" />
      </node>
      <node concept="2bv6ZS" id="3vw$xkYGX1I" role="2bv01j">
        <property role="TrG5h" value="Gezinsinkomen" />
        <node concept="1EDDfm" id="3vw$xkYGXc_" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
      <node concept="2bv6ZS" id="3vw$xkYGXjt" role="2bv01j">
        <property role="TrG5h" value="Gezamenlijk vermogen" />
        <node concept="1EDDfm" id="3vw$xkYGXsT" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
      <node concept="2bv6ZS" id="3vw$xkYH63G" role="2bv01j">
        <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur " />
        <node concept="1EDDfm" id="3vw$xkYH6ve" role="1EDDcc">
          <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
        </node>
      </node>
    </node>
    <node concept="2bv6Zy" id="3l119$$V7zv" role="2bv6Cn">
      <property role="TrG5h" value="bedrag" />
      <node concept="1EDDeX" id="3l119$$VaIA" role="1ECJDa">
        <property role="3GST$d" value="2" />
      </node>
    </node>
    <node concept="2DSAsB" id="6PDUWJHlm3h" role="2bv6Cn">
      <property role="TrG5h" value="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
      <node concept="1EDDfm" id="3l119$$VG8G" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="6PDUWJHlm4V" role="2bv6Cn">
      <property role="TrG5h" value="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
      <node concept="1EDDfm" id="3l119$$VGbN" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="6PDUWJHlm6L" role="2bv6Cn">
      <property role="TrG5h" value="BOVENGRENS_VERMOGEN_LAAG" />
      <node concept="1EDDfm" id="3l119$$VGd8" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="6PDUWJHlm8i" role="2bv6Cn">
      <property role="TrG5h" value="BOVENGRENS_VERMOGEN_HOOG" />
      <node concept="1EDDfm" id="3l119$$VGet" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYy0" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
      <node concept="1EDDfm" id="3vw$xkYGYy1" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGXfm" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
      <node concept="1EDDfm" id="3vw$xkYGXgH" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYDY" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
      <node concept="1EDDfm" id="3vw$xkYGYDZ" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYE0" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
      <node concept="1EDDfm" id="3vw$xkYGYE1" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYIG" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 18-20 ALLEENSTAANDE (OUDER) " />
      <node concept="1EDDfm" id="3vw$xkYGYIH" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYII" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21" />
      <node concept="1EDDfm" id="3vw$xkYGYIJ" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYNc" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21 MET KIND" />
      <node concept="1EDDfm" id="3vw$xkYGYNd" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYOf" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21" />
      <node concept="1EDDfm" id="3vw$xkYGYOg" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGYQJ" role="2bv6Cn">
      <property role="TrG5h" value="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21 MET KIND" />
      <node concept="1EDDfm" id="3vw$xkYGYQK" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGXyQ" role="2bv6Cn">
      <property role="TrG5h" value="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
      <node concept="1EDDfm" id="3vw$xkYGX$d" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
    <node concept="2DSAsB" id="3vw$xkYGX_3" role="2bv6Cn">
      <property role="TrG5h" value="VERMOGENSGRENS ALLEENSTAANDE" />
      <node concept="1EDDfm" id="3vw$xkYGXBo" role="1ERmGI">
        <ref role="1EDDfl" node="3l119$$V7zv" resolve="bedrag" />
      </node>
    </node>
  </node>
</model>

