<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9f7764e1-81c7-4150-a011-8bdc0e8469c7(Regels)">
  <persistence version="9" />
  <languages>
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="6mfl" ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Gegevens)" />
  </imports>
  <registry>
    <language id="7bbaf860-5f96-44b4-9731-6e00ae137ece" name="regelspraak">
      <concept id="3534427357916799793" name="regelspraak.structure.IOnderwerpKetting" flags="ng" index="200mR5">
        <child id="3534427357916799800" name="onderwerp" index="200mRc" />
      </concept>
      <concept id="653687101152476296" name="regelspraak.structure.ActieIndienVoorwaarde" flags="ng" index="2boe1W">
        <child id="1480463129960504796" name="conditie" index="1wO7i3" />
        <child id="1480463129960504793" name="actie" index="1wO7i6" />
      </concept>
      <concept id="653687101152476297" name="regelspraak.structure.Gelijkstelling" flags="ng" index="2boe1X">
        <child id="653687101152498722" name="rechts" index="2bokzm" />
        <child id="653687101152498719" name="links" index="2bokzF" />
      </concept>
      <concept id="653687101152476040" name="regelspraak.structure.ParameterRef" flags="ng" index="2boetW">
        <reference id="653687101152476041" name="param" index="2boetX" />
      </concept>
      <concept id="653687101152474198" name="regelspraak.structure.ZijnAttribuut" flags="ng" index="2boeyy">
        <reference id="653687101152474199" name="attr" index="2boeyz" />
        <reference id="5392295481195528273" name="onderwerp" index="3yjWxU" />
      </concept>
      <concept id="653687101152498727" name="regelspraak.structure.AttribuutVan" flags="ng" index="2bokzj">
        <reference id="653687101152590724" name="attr" index="2bpytK" />
      </concept>
      <concept id="653687101158189440" name="regelspraak.structure.Regelgroep" flags="ng" index="2bQVlO">
        <child id="9154144551704439187" name="regel" index="1HSqhF" />
      </concept>
      <concept id="803952362794499641" name="regelspraak.structure.KenmerkToekenning" flags="ng" index="jASxj">
        <reference id="803952362794499837" name="kenmerk" index="jASyn" />
      </concept>
      <concept id="6223277501270327848" name="regelspraak.structure.AbstracteRegel" flags="ng" index="nISv2">
        <child id="6223277501273432772" name="versie" index="kiesI" />
      </concept>
      <concept id="960165988391648671" name="regelspraak.structure.RolOfKenmerkCheck" flags="ng" index="qbsLY">
        <property id="960165988391648807" name="value" index="qbsZ6" />
        <reference id="4170820228915426206" name="rolOfKenmerk" index="1fps_S" />
        <child id="5286756736493673756" name="onderwerp" index="25Lowx" />
      </concept>
      <concept id="6528193855467691460" name="regelspraak.structure.Onderwerp" flags="ng" index="u$H1X">
        <reference id="6528193855467691461" name="objRolOfKenmerk" index="u$H1W" />
      </concept>
      <concept id="2018749743879756032" name="regelspraak.structure.TekstDeel" flags="ng" index="ymhcM">
        <child id="2018749743879756033" name="waarde" index="ymhcN" />
      </concept>
      <concept id="2800963173589723981" name="regelspraak.structure.EnkelvoudigeConditie" flags="ng" index="2C6MTB">
        <child id="2800963173589723982" name="links" index="2C6MT$" />
      </concept>
      <concept id="7004474094244907415" name="regelspraak.structure.AbstracteRegelVersie" flags="ng" index="2KO2Q4">
        <child id="5118870146818423030" name="geldig" index="1nvPAL" />
      </concept>
      <concept id="1480463129960505090" name="regelspraak.structure.RegelVersie" flags="ng" index="1wO7pt">
        <child id="1480463129960505094" name="statement" index="1wO7pp" />
      </concept>
      <concept id="1480463129961380548" name="regelspraak.structure.Subconditie" flags="ng" index="1wSDer">
        <child id="1480463129961380549" name="conditie" index="1wSDeq" />
      </concept>
      <concept id="1480463129961383198" name="regelspraak.structure.OnderwerpRef" flags="ng" index="1wSIL1">
        <reference id="5392295481204439250" name="onderwerp" index="3zXN3T" />
      </concept>
      <concept id="1480463129961327091" name="regelspraak.structure.SamengesteldeConditie" flags="ng" index="1wSW2G">
        <child id="1480463129961381494" name="onderwerp" index="1wSDsD" />
        <child id="1480463129961327092" name="subconditie" index="1wSW2F" />
        <child id="1480463129962698889" name="quant" index="1wXJ7m" />
      </concept>
      <concept id="1480463129962407221" name="regelspraak.structure.Vergelijking" flags="ng" index="1wWOLE">
        <property id="5970487230361816964" name="operator" index="2noU44" />
        <child id="1480463129962407225" name="rechts" index="1wWOLA" />
      </concept>
      <concept id="1480463129962641110" name="regelspraak.structure.AantalQuantificatie" flags="ng" index="1wXXY9">
        <property id="1480463129962641111" name="aantal" index="1wXXY8" />
      </concept>
      <concept id="1480463129962641080" name="regelspraak.structure.Alle" flags="ng" index="1wXXZB" />
      <concept id="9154144551704438971" name="regelspraak.structure.Regel" flags="ng" index="1HSql3" />
      <concept id="5514682949681279713" name="regelspraak.structure.TekstExpressie" flags="ng" index="3ObYgd">
        <child id="6899278994318486911" name="tekstdeel" index="2x5sjf" />
      </concept>
    </language>
    <language id="08d6f877-03cc-45d3-b03c-d6f786266853" name="bronspraak">
      <concept id="4952724140648782884" name="bronspraak.structure.BronVerwijzingAttribute" flags="ng" index="35pc1T">
        <child id="7387894680620197933" name="verwijzing" index="3qQBGW" />
      </concept>
      <concept id="2303539061403940626" name="bronspraak.structure.VrijeVerwijzing" flags="ng" index="16K2u0">
        <property id="2303539061404573372" name="tekst" index="16H$SI" />
        <property id="2303539061403940629" name="url" index="16K2u7" />
      </concept>
    </language>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="5478077304742291705" name="gegevensspraak.structure.DatumTijdLiteral" flags="ng" index="2ljiaL">
        <property id="5478077304742291708" name="jaar" index="2ljiaO" />
      </concept>
      <concept id="5478077304742085581" name="gegevensspraak.structure.Geldigheidsperiode" flags="ng" index="2ljwA5">
        <child id="5478077304742085582" name="van" index="2ljwA6" />
      </concept>
      <concept id="4697074533531412717" name="gegevensspraak.structure.TekstLiteral" flags="ng" index="2JwNib">
        <property id="4697074533531412721" name="waarde" index="2JwNin" />
      </concept>
      <concept id="4697074533531324619" name="gegevensspraak.structure.BooleanLiteral" flags="ng" index="2Jx4MH">
        <property id="4697074533531324626" name="waarde" index="2Jx4MO" />
      </concept>
      <concept id="558527188464633210" name="gegevensspraak.structure.AbstractNumeriekeLiteral" flags="ng" index="3e5kNY">
        <property id="558527188465081158" name="waarde" index="3e6Tb2" />
      </concept>
      <concept id="5917060184181965945" name="gegevensspraak.structure.NumeriekeLiteral" flags="ng" index="1EQTEq" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="3717301156197626279" name="jetbrains.mps.lang.core.structure.BasePlaceholder" flags="ng" index="3DQ70j">
        <child id="3717301156197626301" name="content" index="3DQ709" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="role_DebugInfo" index="3V$3am" />
      </concept>
    </language>
    <language id="c7fb639f-be78-4307-89b0-b5959c3fa8c8" name="jetbrains.mps.lang.text">
      <concept id="155656958578482948" name="jetbrains.mps.lang.text.structure.Word" flags="nn" index="3oM_SD">
        <property id="155656958578482949" name="value" index="3oM_SC" />
      </concept>
      <concept id="2535923850359206929" name="jetbrains.mps.lang.text.structure.Text" flags="nn" index="1Pa9Pv">
        <child id="2535923850359210936" name="lines" index="1PaQFQ" />
      </concept>
      <concept id="2535923850359271782" name="jetbrains.mps.lang.text.structure.Line" flags="nn" index="1PaTwC">
        <child id="2535923850359271783" name="elements" index="1PaTwD" />
      </concept>
    </language>
  </registry>
  <node concept="2bQVlO" id="6PDUWJHkHID">
    <property role="TrG5h" value="Uit te keren individuele inkomenstoeslag" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <node concept="1HSql3" id="6PDUWJHkHTZ" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 01" />
      <node concept="1wO7pt" id="6PDUWJHkHU0" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkHU1" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkHUN" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkHUO" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="6PDUWJHkHUP" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="6PDUWJHkHVQ" role="2bokzm">
              <property role="3e6Tb2" value="51" />
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkHWB" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkIa5" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkJh7" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkJhU" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkJia" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkHWE" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkIel" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkIem" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkIeP" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkIff" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkIfT" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkIfU" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkIgt" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkJzG" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkJ$b" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkJ$c" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkJ$J" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkJ_b" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkJ_N" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkJ_O" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkJAr" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkJAT" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkJBE" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkJBF" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2boetW" id="6PDUWJHlv1l" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkJEy" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkJFy" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkJFz" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                  <ref role="3yjWxU" node="6PDUWJHkHUP" resolve="Aanvraag" />
                </node>
                <node concept="2boetW" id="6PDUWJHlvkV" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkHU3" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkHUx" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
        <node concept="35pc1T" id="2475v2lZScM" role="lGtFl">
          <node concept="16K2u0" id="2475v2lZSR7" role="3qQBGW">
            <property role="16K2u7" value="https://pki.utrecht.nl/Loket/product/0c17f7cd409dc999eb351883a138ca3d" />
            <property role="16H$SI" value="Individuele inkomenstoeslag aanvragen" />
          </node>
          <node concept="16K2u0" id="2475v2lZVjE" role="3qQBGW">
            <property role="16K2u7" value="https://decentrale.regelgeving.overheid.nl/cvdr/xhtmloutput/Actueel/Utrecht (Utr)/CVDR642743.html" />
            <property role="16H$SI" value="Beleidsregel Individuele Inkomenstoeslag gemeente Utrecht" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkLlb" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 02" />
      <node concept="1wO7pt" id="6PDUWJHkLlc" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkLld" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkLle" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkLlf" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="6PDUWJHkLlg" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="6PDUWJHkLlh" role="2bokzm">
              <property role="3e6Tb2" value="231" />
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkLli" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkLlj" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkLlk" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkLll" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkLlm" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkLlg" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLln" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLlo" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLlp" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLlq" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLlr" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLls" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLlt" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLlu" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLlv" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLlw" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLlx" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLly" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLlz" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLl$" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLl_" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLlA" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLlB" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLlC" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkLlD" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlvC_" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLlF" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLlG" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkLlH" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLlg" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlvWe" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm6L" resolve="BOVENGRENS_VERMOGEN_LAAG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkLlJ" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkLlK" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkLG9" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 03" />
      <node concept="1wO7pt" id="6PDUWJHkLGa" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkLGb" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkLGc" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkLGd" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="6PDUWJHkLGe" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="6PDUWJHkLGf" role="2bokzm">
              <property role="3e6Tb2" value="591" />
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkLGg" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkLGh" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkLGi" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkLGj" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkLGk" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkLGe" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLGl" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGm" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLGn" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLGo" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLGp" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGq" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLGr" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLGs" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLGt" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGu" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLGv" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLGw" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLGx" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGy" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkLGz" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkLG$" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLG_" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGA" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkLGB" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlwfY" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkLGD" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkLGE" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkLGF" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkLGe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlwzK" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkLGH" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkLGI" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkMDx" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 04" />
      <node concept="1wO7pt" id="6PDUWJHkMDy" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkMDz" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkMD$" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkMD_" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="6PDUWJHkMDA" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="6PDUWJHkMDB" role="2bokzm">
              <property role="3e6Tb2" value="591" />
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkMDC" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkMDD" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkMDE" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkMDF" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkMDG" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkMDA" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkMDH" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkMDI" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkMDJ" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkMDK" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkMDL" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkMDM" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkMDN" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkMDO" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkMDP" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkMDQ" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkMDR" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkMDS" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkMDT" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkMDU" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkMDV" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkMDW" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkMDX" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkMDY" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkMDZ" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlwRz" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkME1" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkME2" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkME3" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkMDA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlxbo" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkME5" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkME6" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHl15k" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 05" />
      <node concept="1wO7pt" id="6PDUWJHl15m" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHl15n" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHl1Wc" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHl1Wd" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="6PDUWJHl1We" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="6PDUWJHl28d" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="qbsLY" id="6PDUWJHl2IV" role="1wO7i3">
            <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
            <node concept="1wSIL1" id="6PDUWJHl2Sv" role="25Lowx">
              <ref role="3zXN3T" node="6PDUWJHl1We" resolve="Aanvraag" />
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHl15p" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHl1W4" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$ViC2" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 06" />
      <node concept="1wO7pt" id="3l119$$ViC3" role="kiesI">
        <node concept="2boe1W" id="3l119$$ViC4" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$ViC5" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$ViC6" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$ViC7" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$ViC8" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wWOLE" id="3l119$$VjmD" role="1wO7i3">
            <node concept="2boeyy" id="3l119$$VjmE" role="2C6MT$">
              <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
              <ref role="3yjWxU" node="3l119$$ViC7" resolve="Aanvraag" />
            </node>
            <node concept="2Jx4MH" id="3l119$$Vk2h" role="1wWOLA">
              <property role="2Jx4MO" value="true" />
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$ViCb" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$ViCc" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VkiA" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 07" />
      <node concept="1wO7pt" id="3l119$$VkiB" role="kiesI">
        <node concept="2boe1W" id="3l119$$VkiC" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VkiD" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VkiE" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$VkiF" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$VkiG" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wWOLE" id="3l119$$VkiH" role="1wO7i3">
            <node concept="2boeyy" id="3l119$$VkiI" role="2C6MT$">
              <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
              <ref role="3yjWxU" node="3l119$$VkiF" resolve="Aanvraag" />
            </node>
            <node concept="2Jx4MH" id="3l119$$VkiJ" role="1wWOLA" />
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VkiK" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VkiL" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$Vmr8" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 08" />
      <node concept="1wO7pt" id="3l119$$Vmr9" role="kiesI">
        <node concept="2boe1W" id="3l119$$Vmra" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$Vmrb" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$Vmrc" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$Vmrd" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$Vmre" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$Vowg" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$Vp4G" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$Vowi" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$Vpq1" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$Vpq2" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                  <ref role="3yjWxU" node="3l119$$Vmrd" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3l119$$Vq15" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$Vowj" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$Vqrq" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$Vqrr" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                  <ref role="3yjWxU" node="3l119$$Vmrd" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3l119$$Vr6a" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VrpI" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VrIj" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VrIk" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VrIl" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$Vs4Y" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$Vs4Z" role="2C6MT$">
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                      <ref role="3yjWxU" node="3l119$$Vmrd" resolve="Aanvraag" />
                    </node>
                    <node concept="2boetW" id="3l119$$Vt5E" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VtKa" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VupD" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VupE" role="2C6MT$">
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                      <ref role="3yjWxU" node="3l119$$Vmrd" resolve="Aanvraag" />
                    </node>
                    <node concept="2boetW" id="3l119$$Vvrw" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$Vmri" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$Vmrj" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$Vxqn" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 09" />
      <node concept="1wO7pt" id="3l119$$Vxqo" role="kiesI">
        <node concept="2boe1W" id="3l119$$Vxqp" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$Vxqq" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$Vxqr" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$Vxqs" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$Vxqt" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$Vxqu" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$Vxqv" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$Vxqw" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$Vxqx" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$Vxqy" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$Vxqs" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$Vxqz" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$Vxq$" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$Vxq_" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VxqA" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$Vxqs" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VxqB" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VxqC" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VxqD" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VxqE" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VxqF" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VxqG" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VxqH" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$Vxqs" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$VxqI" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VxqJ" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VxqK" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VxqL" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$Vxqs" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$VxqM" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm6L" resolve="BOVENGRENS_VERMOGEN_LAAG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VxqN" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VxqO" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VzV_" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 10" />
      <node concept="1wO7pt" id="3l119$$VzVA" role="kiesI">
        <node concept="2boe1W" id="3l119$$VzVB" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VzVC" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VzVD" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$VzVE" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$VzVF" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$VzVG" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$VzVH" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$VzVI" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VzVJ" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VzVK" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VzVE" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VzVL" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VzVM" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VzVN" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VzVO" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VzVE" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VzVP" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VzVQ" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VzVR" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VzVS" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VzVT" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VzVU" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VzVV" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VzVE" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$VzVW" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VzVX" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VzVY" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VzVZ" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VzVE" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$VzW0" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VzW1" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VzW2" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VBAx" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren toeslag bedrag 11" />
      <node concept="1wO7pt" id="3l119$$VBAy" role="kiesI">
        <node concept="2boe1W" id="3l119$$VBAz" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VBA$" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VBA_" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
              <node concept="u$H1X" id="3l119$$VBAA" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3l119$$VBAB" role="2bokzm">
              <property role="3e6Tb2" value="0" />
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$VBAC" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$VBAD" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$VBAE" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VBAF" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VBAG" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VBAA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VBAH" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VBAI" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VBAJ" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VBAK" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VBAA" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VBAL" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VBAM" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VBAN" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VBAO" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VBAP" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VBAQ" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VBAR" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VBAA" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$VBAS" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VBAT" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VBAU" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VBAV" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VBAA" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$VBAW" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VBAX" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VBAY" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2bQVlO" id="6PDUWJHkKMM">
    <property role="TrG5h" value="Toekennen kenmerken Utrecht" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <node concept="1HSql3" id="6PDUWJHkKMN" role="1HSqhF">
      <property role="TrG5h" value="Aanvrager die woonachtig is in Utrecht 01" />
      <node concept="1wO7pt" id="6PDUWJHkKMO" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkKMP" role="1wO7pp">
          <node concept="jASxj" id="6PDUWJHkKNi" role="1wO7i6">
            <ref role="jASyn" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
            <node concept="u$H1X" id="6PDUWJHkKNs" role="200mRc">
              <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkKPI" role="1wO7i3">
            <node concept="1wSIL1" id="6PDUWJHkKPJ" role="1wSDsD">
              <ref role="3zXN3T" node="6PDUWJHkKNs" resolve="Aanvraag" />
            </node>
            <node concept="1wXXY9" id="6PDUWJHkKQv" role="1wXJ7m">
              <property role="1wXXY8" value="1" />
            </node>
            <node concept="1wSDer" id="6PDUWJHkKPL" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkKNB" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkKNC" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
                  <ref role="3yjWxU" node="6PDUWJHkKNs" resolve="Aanvraag" />
                </node>
                <node concept="3ObYgd" id="6PDUWJHkKOn" role="1wWOLA">
                  <node concept="ymhcM" id="6PDUWJHkKOm" role="2x5sjf">
                    <node concept="2JwNib" id="6PDUWJHkKOl" role="ymhcN">
                      <property role="2JwNin" value="utrecht" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkKPM" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkKQG" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkKQH" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
                  <ref role="3yjWxU" node="6PDUWJHkKNs" resolve="Aanvraag" />
                </node>
                <node concept="3ObYgd" id="6PDUWJHkKRA" role="1wWOLA">
                  <node concept="ymhcM" id="6PDUWJHkKR_" role="2x5sjf">
                    <node concept="2JwNib" id="6PDUWJHkKR$" role="ymhcN">
                      <property role="2JwNin" value="Utrecht" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkKMR" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkKN8" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2bQVlO" id="6PDUWJHkRXc">
    <property role="TrG5h" value="Recht beschrijving" />
    <property role="3GE5qa" value="Individuele Inkomenstoeslag" />
    <node concept="1HSql3" id="6PDUWJHkRXd" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 01" />
      <node concept="1wO7pt" id="6PDUWJHkRXe" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkRXf" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkRXg" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkRXh" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="6PDUWJHkRXi" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="6PDUWJHkTGX" role="2bokzm">
              <node concept="ymhcM" id="6PDUWJHkTGW" role="2x5sjf">
                <node concept="2JwNib" id="6PDUWJHkTGV" role="ymhcN">
                  <property role="2JwNin" value="Recht op IIT €51" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkRXk" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkRXl" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkRXm" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkRXn" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkRXo" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkRXi" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXp" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXq" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRXr" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRXs" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXt" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXu" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRXv" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRXw" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXx" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXy" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRXz" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRX$" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRX_" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXA" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRXB" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRXC" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXD" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXE" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRXF" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlmvz" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXH" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRXI" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRXJ" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlmN9" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkRXL" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkRXM" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkRXN" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 02" />
      <node concept="1wO7pt" id="6PDUWJHkRXO" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkRXP" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkRXQ" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkRXR" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="6PDUWJHkRXS" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="6PDUWJHkUKx" role="2bokzm">
              <node concept="ymhcM" id="6PDUWJHkUKw" role="2x5sjf">
                <node concept="2JwNib" id="6PDUWJHkUKv" role="ymhcN">
                  <property role="2JwNin" value="Recht op IIT €231" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkRXU" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkRXV" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkRXW" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkRXX" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkRXY" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkRXS" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRXZ" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRY0" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRY1" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRY2" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRY3" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRY4" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRY5" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRY6" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRY7" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRY8" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRY9" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYa" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYb" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYc" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRYd" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYe" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYf" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYg" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRYh" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHln6N" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYj" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYk" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRYl" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRXS" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlnqs" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm6L" resolve="BOVENGRENS_VERMOGEN_LAAG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkRYn" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkRYo" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkRYp" role="1HSqhF">
      <property role="TrG5h" value="URecht beschrijving 03" />
      <node concept="1wO7pt" id="6PDUWJHkRYq" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkRYr" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkRYs" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkRYt" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="6PDUWJHkRYu" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="6PDUWJHkVdn" role="2bokzm">
              <node concept="ymhcM" id="6PDUWJHkVdm" role="2x5sjf">
                <node concept="2JwNib" id="6PDUWJHkVdl" role="ymhcN">
                  <property role="2JwNin" value="Recht op IIT €591" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkRYw" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkRYx" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkRYy" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkRYz" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkRY$" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkRYu" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRY_" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYA" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRYB" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYC" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYD" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYE" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRYF" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYG" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYH" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYI" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRYJ" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYK" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYL" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYM" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRYN" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRYO" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYP" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYQ" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRYR" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlnIc" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRYT" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRYU" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRYV" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRYu" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHlo1Y" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkRYX" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkRYY" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHkRYZ" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 04" />
      <node concept="1wO7pt" id="6PDUWJHkRZ0" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHkRZ1" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHkRZ2" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHkRZ3" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="6PDUWJHkRZ4" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="6PDUWJHkVPQ" role="2bokzm">
              <node concept="ymhcM" id="6PDUWJHkVPP" role="2x5sjf">
                <node concept="2JwNib" id="6PDUWJHkVPO" role="ymhcN">
                  <property role="2JwNin" value="Recht op IIT €591" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="6PDUWJHkRZ6" role="1wO7i3">
            <node concept="1wXXZB" id="6PDUWJHkRZ7" role="1wXJ7m" />
            <node concept="1wSDer" id="6PDUWJHkRZ8" role="1wSW2F">
              <node concept="qbsLY" id="6PDUWJHkRZ9" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
                <node concept="1wSIL1" id="6PDUWJHkRZa" role="25Lowx">
                  <ref role="3zXN3T" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZb" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZc" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRZd" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRZe" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZf" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZg" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRZh" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRZi" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZj" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZk" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRZl" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRZm" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZn" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZo" role="1wSDeq">
                <node concept="2boeyy" id="6PDUWJHkRZp" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="6PDUWJHkRZq" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZr" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZs" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRZt" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                </node>
                <node concept="2boetW" id="6PDUWJHlolL" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="6PDUWJHkRZv" role="1wSW2F">
              <node concept="1wWOLE" id="6PDUWJHkRZw" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="6PDUWJHkRZx" role="2C6MT$">
                  <ref role="3yjWxU" node="6PDUWJHkRZ4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                </node>
                <node concept="2boetW" id="6PDUWJHloDA" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHkRZz" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHkRZ$" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="6PDUWJHl4jo" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 05" />
      <node concept="1wO7pt" id="6PDUWJHl4jq" role="kiesI">
        <node concept="2boe1W" id="6PDUWJHl4jr" role="1wO7pp">
          <node concept="2boe1X" id="6PDUWJHl4VR" role="1wO7i6">
            <node concept="2bokzj" id="6PDUWJHl4VS" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="6PDUWJHl4VT" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="6PDUWJHl5aM" role="2bokzm">
              <node concept="ymhcM" id="6PDUWJHl5aL" role="2x5sjf">
                <node concept="2JwNib" id="6PDUWJHl5aK" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="qbsLY" id="6PDUWJHl5wp" role="1wO7i3">
            <ref role="1fps_S" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
            <node concept="1wSIL1" id="6PDUWJHl5Qi" role="25Lowx">
              <ref role="3zXN3T" node="6PDUWJHl4VT" resolve="Aanvraag" />
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="6PDUWJHl4jt" role="1nvPAL">
          <node concept="2ljiaL" id="6PDUWJHl4VF" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VJtM" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 06" />
      <node concept="1wO7pt" id="3l119$$VJtN" role="kiesI">
        <node concept="2boe1W" id="3l119$$VJtO" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VJtP" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VJtQ" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$VJtR" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$VJtS" role="2bokzm">
              <node concept="ymhcM" id="3l119$$VJtT" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$VJtU" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wWOLE" id="3l119$$VLg0" role="1wO7i3">
            <node concept="2boeyy" id="3l119$$VLg1" role="2C6MT$">
              <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
              <ref role="3yjWxU" node="3l119$$VJtR" resolve="Aanvraag" />
            </node>
            <node concept="2Jx4MH" id="3l119$$VLTH" role="1wWOLA">
              <property role="2Jx4MO" value="true" />
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VJtX" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VJtY" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VM9s" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 07" />
      <node concept="1wO7pt" id="3l119$$VM9t" role="kiesI">
        <node concept="2boe1W" id="3l119$$VM9u" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VM9v" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VM9w" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$VM9x" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$VM9y" role="2bokzm">
              <node concept="ymhcM" id="3l119$$VM9z" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$VM9$" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wWOLE" id="3l119$$VM9_" role="1wO7i3">
            <node concept="2boeyy" id="3l119$$VM9A" role="2C6MT$">
              <ref role="2boeyz" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
              <ref role="3yjWxU" node="3l119$$VM9x" resolve="Aanvraag" />
            </node>
            <node concept="2Jx4MH" id="3l119$$VM9B" role="1wWOLA" />
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VM9C" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VM9D" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VNmv" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 08" />
      <node concept="1wO7pt" id="3l119$$VNmw" role="kiesI">
        <node concept="2boe1W" id="3l119$$VNmx" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VNmy" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VNmz" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$VNm$" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$VNm_" role="2bokzm">
              <node concept="ymhcM" id="3l119$$VNmA" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$VNmB" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$VO3K" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$VO_Y" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$VO3M" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VOTx" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VOTy" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                  <ref role="3yjWxU" node="3l119$$VNm$" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VPxL" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VO3N" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VPRW" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VPRX" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                  <ref role="3yjWxU" node="3l119$$VNm$" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VQRb" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VRb_" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VRv$" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VRv_" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VRvA" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VROt" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VROu" role="2C6MT$">
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                      <ref role="3yjWxU" node="3l119$$VNm$" resolve="Aanvraag" />
                    </node>
                    <node concept="2boetW" id="3l119$$VSP9" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VRvB" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VTzd" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VTze" role="2C6MT$">
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                      <ref role="3yjWxU" node="3l119$$VNm$" resolve="Aanvraag" />
                    </node>
                    <node concept="2boetW" id="3l119$$VU_4" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VNmF" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VNmG" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VViY" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 09" />
      <node concept="1wO7pt" id="3l119$$VViZ" role="kiesI">
        <node concept="2boe1W" id="3l119$$VVj0" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VVj1" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VVj2" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$VVj3" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$VVj4" role="2bokzm">
              <node concept="ymhcM" id="3l119$$VVj5" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$VVj6" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$VVj7" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$VVj8" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$VVj9" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VVja" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VVjb" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VVj3" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VVjc" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VVjd" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VVje" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VVjf" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VVj3" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VVjg" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VVjh" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VVji" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VVjj" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VVjk" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VVjl" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VVjm" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VVj3" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$VVjn" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm3h" resolve="BOVENGRENS_INKOMEN_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VVjo" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VVjp" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VVjq" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VVj3" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$VVjr" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm6L" resolve="BOVENGRENS_VERMOGEN_LAAG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VVjs" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VVjt" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$VX32" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 10" />
      <node concept="1wO7pt" id="3l119$$VX33" role="kiesI">
        <node concept="2boe1W" id="3l119$$VX34" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$VX35" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$VX36" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$VX37" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$VX38" role="2bokzm">
              <node concept="ymhcM" id="3l119$$VX39" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$VX3a" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$VX3b" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$VX3c" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$VX3d" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VX3e" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VX3f" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VX37" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VX3g" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VX3h" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$VX3i" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$VX3j" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$VX37" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$VX3k" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$VX3l" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$VX3m" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$VX3n" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$VX3o" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VX3p" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VX3q" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VX37" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$VX3r" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$VX3s" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$VX3t" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$VX3u" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$VX37" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$VX3v" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$VX3w" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$VX3x" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3l119$$W2lT" role="1HSqhF">
      <property role="TrG5h" value="Recht beschrijving 11" />
      <node concept="1wO7pt" id="3l119$$W2lU" role="kiesI">
        <node concept="2boe1W" id="3l119$$W2lV" role="1wO7pp">
          <node concept="2boe1X" id="3l119$$W2lW" role="1wO7i6">
            <node concept="2bokzj" id="3l119$$W2lX" role="2bokzF">
              <ref role="2bpytK" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
              <node concept="u$H1X" id="3l119$$W2lY" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="3ObYgd" id="3l119$$W2lZ" role="2bokzm">
              <node concept="ymhcM" id="3l119$$W2m0" role="2x5sjf">
                <node concept="2JwNib" id="3l119$$W2m1" role="ymhcN">
                  <property role="2JwNin" value="U heeft geen recht op IIT" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1wSW2G" id="3l119$$W2m2" role="1wO7i3">
            <node concept="1wXXZB" id="3l119$$W2m3" role="1wXJ7m" />
            <node concept="1wSDer" id="3l119$$W2m4" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$W2m5" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$W2m6" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$W2lY" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
                </node>
                <node concept="2Jx4MH" id="3l119$$W2m7" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$W2m8" role="1wSW2F">
              <node concept="1wWOLE" id="3l119$$W2m9" role="1wSDeq">
                <node concept="2boeyy" id="3l119$$W2ma" role="2C6MT$">
                  <ref role="3yjWxU" node="3l119$$W2lY" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
                </node>
                <node concept="2Jx4MH" id="3l119$$W2mb" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3l119$$W2mc" role="1wSW2F">
              <node concept="1wSW2G" id="3l119$$W2md" role="1wSDeq">
                <node concept="1wXXY9" id="3l119$$W2me" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3l119$$W2mf" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$W2mg" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$W2mh" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$W2lY" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
                    </node>
                    <node concept="2boetW" id="3l119$$W2mi" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm4V" resolve="BOVENGRENS_INKOMEN_NIET_ALLEENSTAANDE" />
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="3l119$$W2mj" role="1wSW2F">
                  <node concept="1wWOLE" id="3l119$$W2mk" role="1wSDeq">
                    <property role="2noU44" value="5brrC35IcX$/GT" />
                    <node concept="2boeyy" id="3l119$$W2ml" role="2C6MT$">
                      <ref role="3yjWxU" node="3l119$$W2lY" resolve="Aanvraag" />
                      <ref role="2boeyz" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
                    </node>
                    <node concept="2boetW" id="3l119$$W2mm" role="1wWOLA">
                      <ref role="2boetX" to="6mfl:6PDUWJHlm8i" resolve="BOVENGRENS_VERMOGEN_HOOG" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3l119$$W2mn" role="1nvPAL">
          <node concept="2ljiaL" id="3l119$$W2mo" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2bQVlO" id="3vw$xkYH054">
    <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur" />
    <property role="3GE5qa" value="Bijdrage Sport en Cultuur" />
    <node concept="1HSql3" id="3vw$xkYH861" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 01" />
      <node concept="1wO7pt" id="3vw$xkYH862" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYH863" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYH86s" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYH86t" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYH86u" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYH86R" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYH87o" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIGPz" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYH969" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYH96r" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYH96_" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYH86u" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYH87q" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYH8lY" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYH8m6" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                  <ref role="3yjWxU" node="3vw$xkYH86u" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYH8ms" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYH98R" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYH99n" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYH99B" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                  <ref role="3yjWxU" node="3vw$xkYH86u" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYH9ad" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHzR_" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYH$jz" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYH$j$" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYH86u" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYH_b8" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYH87r" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYH96T" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYH975" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                  <ref role="3yjWxU" node="3vw$xkYH86u" resolve="Aanvraag" />
                </node>
                <node concept="2boetW" id="3vw$xkYH985" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYIG" resolve="120% VAN DE BIJSTANDSNORM 18-20 ALLEENSTAANDE (OUDER) " />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcpd" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcpQ" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHcpR" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                  <ref role="3yjWxU" node="3vw$xkYH86u" resolve="Aanvraag" />
                </node>
                <node concept="2boetW" id="3vw$xkYHcqX" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGX_3" resolve="VERMOGENSGRENS ALLEENSTAANDE" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYH865" role="1nvPAL" />
        <node concept="35pc1T" id="2475v2lZF6t" role="lGtFl">
          <node concept="16K2u0" id="2475v2lZHkK" role="3qQBGW">
            <property role="16K2u7" value="https://www.amersfoort.nl/werk-en-uitkering/werk-en-uitkering-tonen-op/bijdrage-voor-sport-en-cultuur-voor-volwassenen-.htm" />
            <property role="16H$SI" value="Bijdrage voor sport en cultuur voor volwassenen" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHcsd" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 02" />
      <node concept="1wO7pt" id="3vw$xkYHcse" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHcsf" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHcsg" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHcsh" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHcsi" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHcsj" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHcsk" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIHrT" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHcsm" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHcsn" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHcso" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHcsi" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcsp" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcsq" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHcsr" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHcsi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHcss" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcst" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcsu" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHcsv" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHcsi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHcsw" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYH_Bh" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHA3H" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHA3I" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHcsi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHAWf" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcsx" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcsy" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHcsz" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHcsi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHcs$" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYIG" resolve="120% VAN DE BIJSTANDSNORM 18-20 ALLEENSTAANDE (OUDER) " />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcs_" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcsA" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHcsB" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHcsi" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHcsC" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHcsD" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHc_K" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 03" />
      <node concept="1wO7pt" id="3vw$xkYHc_L" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHc_M" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHc_N" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHc_O" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHc_P" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHc_Q" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHc_R" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYII2f" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHc_T" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHc_U" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHc_V" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHc_P" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHc_W" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHc_X" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHc_Y" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHc_Z" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcA0" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcA1" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHcA2" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHcA3" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdgk" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdk7" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHdnv" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHdup" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHDD6" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHE6_" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHE6A" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHF11" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcA4" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcA5" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHcA6" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHcA7" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYII" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHcA8" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHcA9" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHcAa" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHc_P" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHcAb" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHcAc" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHdyn" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 04" />
      <node concept="1wO7pt" id="3vw$xkYHdyo" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHdyp" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHdyq" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHdyr" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHdys" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHdyt" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHdyu" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIICC" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHdyw" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHdyx" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHdyy" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHdys" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdyz" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdy$" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHdy_" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHdyA" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdyB" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdyC" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHdyD" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHdyE" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdyF" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdyG" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHdyH" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHdyI" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHBPn" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHCio" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHCip" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHDbR" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdyJ" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdyK" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHdyL" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHdyM" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYNc" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD BEIDE JONGER DAN 21 MET KIND" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHdyN" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHdyO" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHdyP" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHdys" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHdyQ" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHdyR" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHe$_" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 05" />
      <node concept="1wO7pt" id="3vw$xkYHe$A" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHe$B" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHe$C" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHe$D" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHe$E" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHe$F" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHe$G" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIJf1" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHe$I" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHe$J" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHe$K" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHe$E" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHf8Q" role="1wSW2F">
              <node concept="1wSW2G" id="3vw$xkYHfjI" role="1wSDeq">
                <node concept="1wXXY9" id="3vw$xkYHfjJ" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3vw$xkYHe$L" role="1wSW2F">
                  <node concept="1wSW2G" id="2bFjX$n__O5" role="1wSDeq">
                    <node concept="1wSIL1" id="2bFjX$n__O6" role="1wSDsD">
                      <ref role="3zXN3T" node="3vw$xkYHe$E" resolve="Aanvraag" />
                    </node>
                    <node concept="1wXXZB" id="2bFjX$n__O7" role="1wXJ7m" />
                    <node concept="1wSDer" id="2bFjX$n__O8" role="1wSW2F">
                      <node concept="1wWOLE" id="3vw$xkYHe$M" role="1wSDeq">
                        <node concept="2boeyy" id="3vw$xkYHe$N" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                          <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="3vw$xkYHe$O" role="1wWOLA">
                          <property role="2Jx4MO" value="true" />
                        </node>
                      </node>
                    </node>
                    <node concept="1wSDer" id="3vw$xkYHe$T" role="1wSW2F">
                      <node concept="1wWOLE" id="3vw$xkYHe$U" role="1wSDeq">
                        <node concept="2boeyy" id="3vw$xkYHe$V" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                          <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="3vw$xkYHe$W" role="1wWOLA" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="2bFjX$n__O9" role="1wSW2F">
                  <node concept="1wSW2G" id="2bFjX$n_Fir" role="1wSDeq">
                    <node concept="1wXXZB" id="2bFjX$n_GoE" role="1wXJ7m" />
                    <node concept="1wSDer" id="2bFjX$n_Fit" role="1wSW2F">
                      <node concept="1wWOLE" id="2bFjX$n_HuO" role="1wSDeq">
                        <node concept="2boeyy" id="2bFjX$n_HuP" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                          <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="2bFjX$n_JFY" role="1wWOLA" />
                      </node>
                    </node>
                    <node concept="1wSDer" id="2bFjX$n_Fiu" role="1wSW2F">
                      <node concept="1wWOLE" id="2bFjX$n_KNk" role="1wSDeq">
                        <node concept="2boeyy" id="2bFjX$n_KNl" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                          <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="2bFjX$n_N1y" role="1wWOLA">
                          <property role="2Jx4MO" value="true" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHe$P" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHe$Q" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHe$R" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHe$S" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHGUA" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHGUB" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHGUC" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHGUD" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHe$X" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHe$Y" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHe$Z" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHe_0" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYOf" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHe_1" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHe_2" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHe_3" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHe$E" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHe_4" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHe_5" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHhmt" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 06" />
      <node concept="1wO7pt" id="3vw$xkYHhmu" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHhmv" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHhmw" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHhmx" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHhmy" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHhmz" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHhm$" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIJPq" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHhmA" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHhmB" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHhmC" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHhmy" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHhmD" role="1wSW2F">
              <node concept="1wSW2G" id="3vw$xkYHhmE" role="1wSDeq">
                <node concept="1wXXY9" id="3vw$xkYHhmF" role="1wXJ7m">
                  <property role="1wXXY8" value="1" />
                </node>
                <node concept="1wSDer" id="3vw$xkYHhmG" role="1wSW2F">
                  <node concept="1wSW2G" id="2bFjX$n_O9h" role="1wSDeq">
                    <node concept="1wSIL1" id="2bFjX$n_O9i" role="1wSDsD">
                      <ref role="3zXN3T" node="3vw$xkYHhmy" resolve="Aanvraag" />
                    </node>
                    <node concept="1wXXZB" id="2bFjX$n_O9j" role="1wXJ7m" />
                    <node concept="1wSDer" id="2bFjX$n_O9k" role="1wSW2F">
                      <node concept="1wWOLE" id="3vw$xkYHhmH" role="1wSDeq">
                        <node concept="2boeyy" id="3vw$xkYHhmI" role="2C6MT$">
                          <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                          <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                        </node>
                        <node concept="2Jx4MH" id="3vw$xkYHhmJ" role="1wWOLA">
                          <property role="2Jx4MO" value="true" />
                        </node>
                      </node>
                    </node>
                    <node concept="1wSDer" id="3vw$xkYHhmK" role="1wSW2F">
                      <node concept="1wWOLE" id="3vw$xkYHhmL" role="1wSDeq">
                        <node concept="2boeyy" id="3vw$xkYHhmM" role="2C6MT$">
                          <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                          <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                        </node>
                        <node concept="2Jx4MH" id="3vw$xkYHhmN" role="1wWOLA" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1wSDer" id="2bFjX$n_O9l" role="1wSW2F">
                  <node concept="1wSW2G" id="2bFjX$n_TOJ" role="1wSDeq">
                    <node concept="1wXXZB" id="2bFjX$n_UXD" role="1wXJ7m" />
                    <node concept="1wSDer" id="2bFjX$n_TOL" role="1wSW2F">
                      <node concept="1wWOLE" id="2bFjX$n_W6s" role="1wSDeq">
                        <node concept="2boeyy" id="2bFjX$n_W6t" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
                          <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="2bFjX$n_YoT" role="1wWOLA" />
                      </node>
                    </node>
                    <node concept="1wSDer" id="2bFjX$n_TOM" role="1wSW2F">
                      <node concept="1wWOLE" id="2bFjX$n_ZyX" role="1wSDeq">
                        <node concept="2boeyy" id="2bFjX$n_ZyY" role="2C6MT$">
                          <ref role="2boeyz" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
                          <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                        </node>
                        <node concept="2Jx4MH" id="2bFjX$nA1Qu" role="1wWOLA">
                          <property role="2Jx4MO" value="true" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHhmO" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHhmP" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHhmQ" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHhmR" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHHR8" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHHR9" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHHRa" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHHRb" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHhmS" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHhmT" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHhmU" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHhmV" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYQJ" resolve="120% VAN DE BIJSTANDSNORM 18-20 GEHUWD EEN JONGER DAN 21 MET KIND" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHhmW" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHhmX" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHhmY" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHhmy" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHhmZ" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHhn0" role="1nvPAL" />
      </node>
    </node>
    <node concept="3DQ70j" id="3vw$xkYHqTH" role="lGtFl">
      <property role="3V$3am" value="regel" />
      <property role="3V$3ak" value="7bbaf860-5f96-44b4-9731-6e00ae137ece/653687101158189440/9154144551704439187" />
      <node concept="1Pa9Pv" id="3vw$xkYHqTI" role="3DQ709">
        <node concept="1PaTwC" id="3vw$xkYHqTJ" role="1PaQFQ">
          <node concept="3oM_SD" id="3vw$xkYHqTK" role="1PaTwD">
            <property role="3oM_SC" value="Hier" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrrm" role="1PaTwD">
            <property role="3oM_SC" value="ontstaan" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrrp" role="1PaTwD">
            <property role="3oM_SC" value="vragen" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrrt" role="1PaTwD">
            <property role="3oM_SC" value="over" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrry" role="1PaTwD">
            <property role="3oM_SC" value="het" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrrC" role="1PaTwD">
            <property role="3oM_SC" value="verschil" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHMVd" role="1PaTwD">
            <property role="3oM_SC" value="tussen" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHrsG" role="1PaTwD">
            <property role="3oM_SC" value="gezin" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHMV1" role="1PaTwD">
            <property role="3oM_SC" value="en" />
          </node>
          <node concept="3oM_SD" id="3vw$xkYHMUO" role="1PaTwD">
            <property role="3oM_SC" value="gehuwd" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHiBo" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 07" />
      <node concept="1wO7pt" id="3vw$xkYHiBp" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHiBq" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHiBr" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHiBs" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHiBt" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHiBu" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHiBv" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIOng" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHiBx" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHiBy" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHiBz" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHiBt" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHk3l" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHkob" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHkoc" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
                  <ref role="3yjWxU" node="3vw$xkYHiBt" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHl1G" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHMVo" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHNqa" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHNqb" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                  <ref role="3yjWxU" node="3vw$xkYHiBt" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHOnr" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHiBN" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHiBO" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHiBP" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHiBt" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHiBQ" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYy0" resolve="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHiBR" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHiBS" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHiBT" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHiBt" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHiBU" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHiBV" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHmou" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 08" />
      <node concept="1wO7pt" id="3vw$xkYHmov" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHmow" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHmox" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHmoy" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHmoz" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHmo$" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHmo_" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIKrN" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHmoB" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHmoC" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHmoD" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHmoz" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHmoE" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHmoF" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHmoG" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHmoz" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHmoH" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHiBJ" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHiBK" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHiBL" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                  <ref role="3yjWxU" node="3vw$xkYHmoz" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHiBM" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHPlL" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHPP8" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHPP9" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHmoz" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHQNn" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHmoM" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHmoN" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHmoO" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHmoz" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHmoP" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXfm" resolve="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHmoQ" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHmoR" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHmoS" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHmoz" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHmoT" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGX_3" resolve="VERMOGENSGRENS ALLEENSTAANDE" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHmoU" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="3vw$xkYHUh9" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 09" />
      <node concept="1wO7pt" id="3vw$xkYHUha" role="kiesI">
        <node concept="2boe1W" id="3vw$xkYHUhb" role="1wO7pp">
          <node concept="2boe1X" id="3vw$xkYHUhc" role="1wO7i6">
            <node concept="2bokzj" id="3vw$xkYHUhd" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="3vw$xkYHUhe" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="3vw$xkYHUhf" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="3vw$xkYHUhg" role="1wO7i3">
            <node concept="1wXXZB" id="3vw$xkYIL29" role="1wXJ7m" />
            <node concept="1wSDer" id="3vw$xkYHUhi" role="1wSW2F">
              <node concept="qbsLY" id="3vw$xkYHUhj" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="3vw$xkYHUhk" role="25Lowx">
                  <ref role="3zXN3T" node="3vw$xkYHUhe" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHUhl" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHUhm" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHUhn" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHUhe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHUho" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHUhp" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHUhq" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHUhr" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHUhe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHUhs" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHUht" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHUhu" role="1wSDeq">
                <node concept="2boeyy" id="3vw$xkYHUhv" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHUhe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                </node>
                <node concept="2Jx4MH" id="3vw$xkYHWE7" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHUhx" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHUhy" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHUhz" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHUhe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHUh$" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXfm" resolve="120% VAN DE BIJSTANDSNORM 21 TOT AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="3vw$xkYHUh_" role="1wSW2F">
              <node concept="1wWOLE" id="3vw$xkYHUhA" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="3vw$xkYHUhB" role="2C6MT$">
                  <ref role="3yjWxU" node="3vw$xkYHUhe" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="3vw$xkYHUhC" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="3vw$xkYHUhD" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="1qYcgYdXSeZ" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 10" />
      <node concept="1wO7pt" id="1qYcgYdXSf0" role="kiesI">
        <node concept="2boe1W" id="1qYcgYdXSf1" role="1wO7pp">
          <node concept="2boe1X" id="1qYcgYdXSf2" role="1wO7i6">
            <node concept="2bokzj" id="1qYcgYdXSf3" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="1qYcgYdXSf4" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="1qYcgYdXSf5" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="1qYcgYdXSf6" role="1wO7i3">
            <node concept="1wXXZB" id="1qYcgYdXSf7" role="1wXJ7m" />
            <node concept="1wSDer" id="1qYcgYdXSf8" role="1wSW2F">
              <node concept="qbsLY" id="1qYcgYdXSf9" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="1qYcgYdXSfa" role="25Lowx">
                  <ref role="3zXN3T" node="1qYcgYdXSf4" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdXSfb" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdXSfc" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdXSfd" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                  <ref role="3yjWxU" node="1qYcgYdXSf4" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdXSfe" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdXSff" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdXSfg" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdXSfh" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                  <ref role="3yjWxU" node="1qYcgYdXSf4" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdXSfi" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdXYsT" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdXZcu" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdXZcv" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                  <ref role="3yjWxU" node="1qYcgYdXSf4" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdY0Fc" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdXSfn" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdXSfo" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdXSfp" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdXSf4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="1qYcgYdXSfq" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYDY" resolve="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD GEZIN of SAMENWONENDE" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdXSfr" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdXSfs" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdXSft" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdXSf4" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="1qYcgYdXSfu" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="1qYcgYdXSfv" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="1qYcgYdY8bC" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 11" />
      <node concept="1wO7pt" id="1qYcgYdY8bD" role="kiesI">
        <node concept="2boe1W" id="1qYcgYdY8bE" role="1wO7pp">
          <node concept="2boe1X" id="1qYcgYdY8bF" role="1wO7i6">
            <node concept="2bokzj" id="1qYcgYdY8bG" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="1qYcgYdY8bH" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="1qYcgYdY8bI" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="1qYcgYdY8bJ" role="1wO7i3">
            <node concept="1wXXZB" id="1qYcgYdY8bK" role="1wXJ7m" />
            <node concept="1wSDer" id="1qYcgYdY8bL" role="1wSW2F">
              <node concept="qbsLY" id="1qYcgYdY8bM" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="1qYcgYdY8bN" role="25Lowx">
                  <ref role="3zXN3T" node="1qYcgYdY8bH" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdY8bO" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdY8bP" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdY8bQ" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdY8bR" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdY8bS" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdY8bT" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdY8bU" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdY8bV" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdY8bW" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdY8bX" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdY8bY" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdY8bZ" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYbUn" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYcND" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYcNE" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdYe_V" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdY8c0" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdY8c1" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdY8c2" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="1qYcgYdY8c3" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYE0" resolve="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdY8c4" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdY8c5" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdY8c6" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdY8bH" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="1qYcgYdY8c7" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGXyQ" resolve="VERMOGENSGRENS GEZIN, SAMENWONENDE, ALLEENSTAANDE OUDER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="1qYcgYdY8c8" role="1nvPAL" />
      </node>
    </node>
    <node concept="1HSql3" id="1qYcgYdYfwj" role="1HSqhF">
      <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur 12" />
      <node concept="1wO7pt" id="1qYcgYdYfwk" role="kiesI">
        <node concept="2boe1W" id="1qYcgYdYfwl" role="1wO7pp">
          <node concept="2boe1X" id="1qYcgYdYfwm" role="1wO7i6">
            <node concept="2bokzj" id="1qYcgYdYfwn" role="2bokzF">
              <ref role="2bpytK" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
              <node concept="u$H1X" id="1qYcgYdYfwo" role="200mRc">
                <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
              </node>
            </node>
            <node concept="1EQTEq" id="1qYcgYdYfwp" role="2bokzm">
              <property role="3e6Tb2" value="90" />
            </node>
          </node>
          <node concept="1wSW2G" id="1qYcgYdYfwq" role="1wO7i3">
            <node concept="1wXXZB" id="1qYcgYdYfwr" role="1wXJ7m" />
            <node concept="1wSDer" id="1qYcgYdYfws" role="1wSW2F">
              <node concept="qbsLY" id="1qYcgYdYfwt" role="1wSDeq">
                <property role="qbsZ6" value="true" />
                <ref role="1fps_S" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
                <node concept="1wSIL1" id="1qYcgYdYfwu" role="25Lowx">
                  <ref role="3zXN3T" node="1qYcgYdYfwo" resolve="Aanvraag" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwv" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfww" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYfwx" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdYfwy" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwz" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfw$" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYfw_" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdYfwA" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwB" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfwC" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYfwD" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdYfwE" role="1wWOLA">
                  <property role="2Jx4MO" value="true" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwF" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfwG" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYfwH" role="2C6MT$">
                  <ref role="2boeyz" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                </node>
                <node concept="2Jx4MH" id="1qYcgYdYfwI" role="1wWOLA" />
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwJ" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfwK" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdYfwL" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
                </node>
                <node concept="2boetW" id="1qYcgYdYfwM" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGYE0" resolve="120% VAN DE BIJSTANDSNORM VANAF AOW-LEEFTIJD ALLEENSTAANDE (OUDER)" />
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYfwN" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYfwO" role="1wSDeq">
                <property role="2noU44" value="5brrC35IcXw/LE" />
                <node concept="2boeyy" id="1qYcgYdYfwP" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYfwo" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
                </node>
                <node concept="2boetW" id="1qYcgYdYfwQ" role="1wWOLA">
                  <ref role="2boetX" to="6mfl:3vw$xkYGX_3" resolve="VERMOGENSGRENS ALLEENSTAANDE" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="1qYcgYdYfwR" role="1nvPAL" />
      </node>
    </node>
  </node>
  <node concept="2bQVlO" id="1qYcgYdYAKo">
    <property role="TrG5h" value="Toekennen kenmerken Amersfoort" />
    <property role="3GE5qa" value="Bijdrage Sport en Cultuur" />
    <node concept="1HSql3" id="1qYcgYdYAKJ" role="1HSqhF">
      <property role="TrG5h" value="Aanvrager die woonachtig is in Amersfoort 01" />
      <node concept="1wO7pt" id="1qYcgYdYAKK" role="kiesI">
        <node concept="2boe1W" id="1qYcgYdYAKL" role="1wO7pp">
          <node concept="jASxj" id="1qYcgYdYAKM" role="1wO7i6">
            <ref role="jASyn" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
            <node concept="u$H1X" id="1qYcgYdYAKN" role="200mRc">
              <ref role="u$H1W" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
            </node>
          </node>
          <node concept="1wSW2G" id="1qYcgYdYAKO" role="1wO7i3">
            <node concept="1wSIL1" id="1qYcgYdYAKP" role="1wSDsD">
              <ref role="3zXN3T" node="1qYcgYdYAKN" resolve="Aanvraag" />
            </node>
            <node concept="1wXXY9" id="1qYcgYdYAKQ" role="1wXJ7m">
              <property role="1wXXY8" value="1" />
            </node>
            <node concept="1wSDer" id="1qYcgYdYAKR" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYAKS" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYAKT" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYAKN" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
                </node>
                <node concept="3ObYgd" id="1qYcgYdYAKU" role="1wWOLA">
                  <node concept="ymhcM" id="1qYcgYdYAKV" role="2x5sjf">
                    <node concept="2JwNib" id="1qYcgYdYAKW" role="ymhcN">
                      <property role="2JwNin" value="amersfoort" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1wSDer" id="1qYcgYdYAKX" role="1wSW2F">
              <node concept="1wWOLE" id="1qYcgYdYAKY" role="1wSDeq">
                <node concept="2boeyy" id="1qYcgYdYAKZ" role="2C6MT$">
                  <ref role="3yjWxU" node="1qYcgYdYAKN" resolve="Aanvraag" />
                  <ref role="2boeyz" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
                </node>
                <node concept="3ObYgd" id="1qYcgYdYAL0" role="1wWOLA">
                  <node concept="ymhcM" id="1qYcgYdYAL1" role="2x5sjf">
                    <node concept="2JwNib" id="1qYcgYdYAL2" role="ymhcN">
                      <property role="2JwNin" value="Amersfoort" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ljwA5" id="1qYcgYdYAL3" role="1nvPAL">
          <node concept="2ljiaL" id="1qYcgYdYAL4" role="2ljwA6">
            <property role="2ljiaO" value="2021" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

