<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:5354198d-5d59-4b80-a033-3f359688546e(Tests)">
  <persistence version="9" />
  <languages>
    <use id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test" version="5" />
    <devkit ref="d07fa9c5-678d-4a9b-9eaf-b1b8c569b820(alef.devkit)" />
  </languages>
  <imports>
    <import index="dju2" ref="r:9f7764e1-81c7-4150-a011-8bdc0e8469c7(Regels)" />
    <import index="6mfl" ref="r:7240dd22-cd7f-41e2-842a-563e8dc036ef(Gegevens)" />
    <import index="4w08" ref="r:d4c0de40-e3df-4f3d-b55d-4f7e70dbeb0b(Services)" />
    <import index="xshh" ref="r:e66d6402-1c81-4d02-b573-4fd1d2d735a4(Besturing)" />
    <import index="35ve" ref="r:9323a62c-c9bd-4ca3-a074-f1b4670ff741(Parameters)" />
  </imports>
  <registry>
    <language id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test">
      <concept id="5097124989038916362" name="jetbrains.mps.lang.test.structure.TestInfo" flags="ng" index="2XOHcx">
        <property id="5097124989038916363" name="projectPath" index="2XOHcw" />
      </concept>
    </language>
    <language id="7bbaf860-5f96-44b4-9731-6e00ae137ece" name="regelspraak">
      <concept id="2800963173588713245" name="regelspraak.structure.Leeg" flags="ng" index="2CqVCR" />
    </language>
    <language id="8bc962c0-cb3c-49f0-aa03-23c3bc0304b0" name="testspraak">
      <concept id="1132091078824234268" name="testspraak.structure.TestGeval" flags="ng" index="210ffa" />
      <concept id="6527873696160725157" name="testspraak.structure.Resultaat" flags="ng" index="4Oh8J">
        <reference id="6527873696160725158" name="type" index="4Oh8G" />
        <reference id="1509793566137291853" name="instantie" index="3teO_M" />
        <child id="6527873696160725081" name="uitvoer" index="4Ohbj" />
      </concept>
      <concept id="6527873696160724962" name="testspraak.structure.Instantie" flags="ng" index="4OhPC">
        <reference id="6527873696160724967" name="type" index="4OhPH" />
        <child id="6527873696160724965" name="eigenschappen" index="4OhPJ" />
      </concept>
      <concept id="6438506741833356929" name="testspraak.structure.InvoerObject" flags="ng" index="27HnP5">
        <child id="6438506741833356938" name="sub" index="27HnPe" />
      </concept>
      <concept id="6438506741833356942" name="testspraak.structure.UitvoerObject" flags="ng" index="27HnPa">
        <child id="6438506741833356949" name="sub" index="27HnPh" />
      </concept>
      <concept id="7037334947758586263" name="testspraak.structure.TeTestenRegelgroep" flags="ng" index="vfxHe">
        <reference id="7037334947758586267" name="ref" index="vfxH2" />
      </concept>
      <concept id="7760345304267117455" name="testspraak.structure.IAbstractTest" flags="ng" index="10x1HZ">
        <child id="6527873696160724992" name="situatie" index="4Ohaa" />
        <child id="6527873696160725067" name="resultaat" index="4Ohb1" />
      </concept>
      <concept id="8803452945676232781" name="testspraak.structure.ServiceTestSet" flags="ng" index="3dMsQ2">
        <property id="8803452945676232903" name="simpleName" index="3dMsO8" />
        <reference id="8803452945676232789" name="service" index="3dMsQq" />
        <child id="8803452945676245612" name="testGevallen" index="3dMzYz" />
      </concept>
      <concept id="8803452945676232785" name="testspraak.structure.ServiceTest" flags="ng" index="3dMsQu">
        <child id="8803452945676554669" name="invoer" index="3dLJhy" />
        <child id="6404605531306943378" name="voorspelling" index="1GVd_u" />
      </concept>
      <concept id="8803452945675745177" name="testspraak.structure.TestInvoerBericht" flags="ng" index="3dW_9m">
        <property id="8803452945675786235" name="jaar" index="3dWN8O" />
        <child id="8803452945675838248" name="veld" index="3dWWrB" />
      </concept>
      <concept id="8803452945675838251" name="testspraak.structure.TestBerichtVeld" flags="ng" index="3dWWr$">
        <reference id="8803452945675844916" name="veld" index="3dWXzV" />
      </concept>
      <concept id="8803452945675845067" name="testspraak.structure.ComplexTestBerichtVeld" flags="ng" index="3dWXw4">
        <child id="6438506741833356934" name="object" index="27HnP2" />
      </concept>
      <concept id="8803452945675844814" name="testspraak.structure.ElementairTestBerichtVeld" flags="ng" index="3dWX$1">
        <property id="8803452945675844818" name="waarde" index="3dWX$t" />
      </concept>
      <concept id="6363260678693757779" name="testspraak.structure.UitvoerVoorspelling" flags="ng" index="3mzBic">
        <property id="3984684955933690575" name="decimalen" index="V2jGk" />
        <reference id="7760345304268221756" name="eigenschap" index="10Xmnc" />
        <child id="6363260678693757785" name="waarde" index="3mzBi6" />
      </concept>
      <concept id="8931076255651336840" name="testspraak.structure.TestSet" flags="ng" index="1rXTK1">
        <property id="551949645806728613" name="testOpNietVoorspeldeUitvoer" index="1bBDVy" />
        <child id="7037334947758586275" name="teTesten" index="vfxHU" />
        <child id="7760345304265917250" name="testGevallen" index="10_$IM" />
        <child id="5466076230970264373" name="rekendatums" index="1lUMLE" />
        <child id="3279801700007574211" name="geldigheidsperiode" index="3Na4y7" />
      </concept>
      <concept id="3581430746159718484" name="testspraak.structure.EigenschapToekenning" flags="ng" index="3_ceKt">
        <reference id="3581430746159718485" name="eigenschap" index="3_ceKs" />
      </concept>
      <concept id="8511081516214725773" name="testspraak.structure.TeTestenFlow" flags="ng" index="3Eg$c6">
        <reference id="8511081516214725831" name="flow" index="3Eg$dc" />
      </concept>
      <concept id="5917060184176395024" name="testspraak.structure.Toekenning" flags="ng" index="1Er9RN">
        <child id="3581430746159718487" name="waarde" index="3_ceKu" />
      </concept>
      <concept id="6404605531307053492" name="testspraak.structure.TestUitvoerBericht" flags="ng" index="1GVEHS">
        <property id="8691291590219126812" name="aantalInconsistenties" index="1qL7eV" />
        <property id="1749813525838931583" name="consistentieverwachting" index="1Axj1u" />
        <property id="6404605531307070318" name="resultaatcode" index="1GVIAy" />
        <property id="6404605531307070481" name="resultaatmelding" index="1GVIVt" />
        <child id="6404605531307076153" name="veld" index="1GVH3P" />
      </concept>
      <concept id="6404605531307076169" name="testspraak.structure.ComplexeVeldVerwachting" flags="ng" index="1GVH25">
        <child id="6438506741833356945" name="objecten" index="27HnPl" />
      </concept>
      <concept id="6404605531307076159" name="testspraak.structure.ElementaireVeldVerwachting" flags="ng" index="1GVH3N">
        <property id="6404605531307076166" name="waarde" index="1GVH2a" />
      </concept>
      <concept id="6404605531307070317" name="testspraak.structure.VeldVerwachting" flags="ng" index="1GVIAx">
        <reference id="6404605531307076156" name="veld" index="1GVH3K" />
      </concept>
    </language>
    <language id="471364db-8078-4933-b2ef-88232bfa34fc" name="gegevensspraak">
      <concept id="5478077304742291705" name="gegevensspraak.structure.DatumTijdLiteral" flags="ng" index="2ljiaL">
        <property id="5478077304742291706" name="dag" index="2ljiaM" />
        <property id="5478077304742291707" name="maand" index="2ljiaN" />
        <property id="5478077304742291708" name="jaar" index="2ljiaO" />
      </concept>
      <concept id="5478077304742085581" name="gegevensspraak.structure.Geldigheidsperiode" flags="ng" index="2ljwA5">
        <child id="5478077304742085582" name="van" index="2ljwA6" />
        <child id="5478077304742085583" name="tm" index="2ljwA7" />
      </concept>
      <concept id="4697074533531412717" name="gegevensspraak.structure.TekstLiteral" flags="ng" index="2JwNib">
        <property id="4697074533531412721" name="waarde" index="2JwNin" />
      </concept>
      <concept id="4697074533531324619" name="gegevensspraak.structure.BooleanLiteral" flags="ng" index="2Jx4MH">
        <property id="4697074533531324626" name="waarde" index="2Jx4MO" />
      </concept>
      <concept id="558527188464633210" name="gegevensspraak.structure.AbstractNumeriekeLiteral" flags="ng" index="3e5kNY">
        <property id="558527188465081158" name="waarde" index="3e6Tb2" />
      </concept>
      <concept id="5917060184181965945" name="gegevensspraak.structure.NumeriekeLiteral" flags="ng" index="1EQTEq" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2XOHcx" id="6PDUWJHkJQI">
    <property role="2XOHcw" value="." />
  </node>
  <node concept="1rXTK1" id="6PDUWJHkJQD">
    <property role="1bBDVy" value="true" />
    <property role="TrG5h" value="Uit te keren individuele inkomenstoeslag" />
    <property role="3GE5qa" value="Regelgroepen.Individuele Inkomenstoeslag" />
    <node concept="210ffa" id="6PDUWJHkJRa" role="10_$IM">
      <property role="TrG5h" value="001" />
      <node concept="4OhPC" id="6PDUWJHkJRb" role="4Ohaa">
        <property role="TrG5h" value="at1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="6PDUWJHkJRt" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="6PDUWJHkJU$" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJRu" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="6PDUWJHkJV2" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJRw" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="6PDUWJHkJVx" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJRy" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="6PDUWJHkJRz" role="3_ceKu">
            <property role="3e6Tb2" value="1200,00" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJR$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="6PDUWJHkJX2" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJRA" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="6PDUWJHkJXx" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkJRC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="6PDUWJHkJRD" role="3_ceKu">
            <property role="3e6Tb2" value="12400" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="6PDUWJHkJRc" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="6PDUWJHkJYs" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="6PDUWJHkJYQ" role="3mzBi6">
            <property role="3e6Tb2" value="51" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="6PDUWJHkNuD" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="6PDUWJHkNuE" role="4Ohaa">
        <property role="TrG5h" value="at1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="6PDUWJHkNuF" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="6PDUWJHkNuG" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuH" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="6PDUWJHkNuI" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuJ" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="6PDUWJHkNuK" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuL" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="6PDUWJHkNuM" role="3_ceKu">
            <property role="3e6Tb2" value="1200,00" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuN" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="6PDUWJHkNuO" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuP" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="6PDUWJHkNuQ" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkNuR" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="6PDUWJHkNuS" role="3_ceKu">
            <property role="3e6Tb2" value="6224" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="6PDUWJHkNuT" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="6PDUWJHkNuU" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="6PDUWJHkNuV" role="3mzBi6">
            <property role="3e6Tb2" value="231" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="6PDUWJHkR2C" role="10_$IM">
      <property role="TrG5h" value="003" />
      <node concept="4OhPC" id="6PDUWJHkR2D" role="4Ohaa">
        <property role="TrG5h" value="at1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="6PDUWJHkR2E" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="6PDUWJHkR2F" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2G" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="6PDUWJHkR2H" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2I" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="6PDUWJHkR2J" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2K" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="6PDUWJHkR2L" role="3_ceKu">
            <property role="3e6Tb2" value="1724,70" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2M" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="6PDUWJHkR2N" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2O" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="6PDUWJHkR2P" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkR2Q" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="6PDUWJHkR2R" role="3_ceKu">
            <property role="3e6Tb2" value="12450" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="6PDUWJHkR2S" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="6PDUWJHkR2T" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="6PDUWJHkR2U" role="3mzBi6">
            <property role="3e6Tb2" value="591" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="6PDUWJHkRpu" role="10_$IM">
      <property role="TrG5h" value="004" />
      <node concept="4OhPC" id="6PDUWJHkRpv" role="4Ohaa">
        <property role="TrG5h" value="at1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="6PDUWJHkRpw" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="6PDUWJHkRpx" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRpy" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="6PDUWJHkRpz" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRp$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="6PDUWJHkRp_" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRpA" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="6PDUWJHkRpB" role="3_ceKu">
            <property role="3e6Tb2" value="1724,71" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRpC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="6PDUWJHkRpD" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRpE" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="6PDUWJHkRpF" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHkRpG" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="6PDUWJHkRpH" role="3_ceKu">
            <property role="3e6Tb2" value="12450" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="6PDUWJHkRpI" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="6PDUWJHkRpJ" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="6PDUWJHkRpK" role="3mzBi6">
            <property role="3e6Tb2" value="591" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2ljwA5" id="6PDUWJHkJQE" role="3Na4y7">
      <node concept="2ljiaL" id="6PDUWJHkJQF" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="6PDUWJHkJQG" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="6PDUWJHkJQH" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="vfxHe" id="6PDUWJHkJQW" role="vfxHU">
      <ref role="vfxH2" to="dju2:6PDUWJHkHID" resolve="Uit te keren individuele inkomenstoeslag" />
    </node>
  </node>
  <node concept="3dMsQ2" id="6PDUWJHlifx">
    <property role="3dMsO8" value="Servicetest Individuele Inkomenstoeslag" />
    <property role="3GE5qa" value="Services.Individuele Inkomenstoeslag" />
    <ref role="3dMsQq" to="4w08:6PDUWJHl8uC" resolve="IIT" />
    <node concept="3dMsQu" id="6PDUWJHliga" role="3dMzYz">
      <property role="TrG5h" value="Servicetest 01" />
      <node concept="3dW_9m" id="6PDUWJHligc" role="3dLJhy">
        <property role="3dWN8O" value="2021" />
        <node concept="3dWXw4" id="6PDUWJHligd" role="3dWWrB">
          <ref role="3dWXzV" to="4w08:6PDUWJHl8uD" resolve="invoer" />
          <node concept="27HnP5" id="6PDUWJHlige" role="27HnP2">
            <node concept="3dWX$1" id="6PDUWJHligf" role="27HnPe">
              <property role="3dWX$t" value="utrecht" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWt" resolve="woonplaats" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligg" role="27HnPe">
              <property role="3dWX$t" value="false" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWy" resolve="aowLeeftijdBehaald" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligh" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWE" resolve="ouderDan21" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligi" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWO" resolve="alleenstaande" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligj" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXX0" resolve="thuiswonendeKinderen" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligk" role="27HnPe">
              <property role="3dWX$t" value="1207.30" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXXe" resolve="inkomenPerMaand" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHligl" role="27HnPe">
              <property role="3dWX$t" value="12450" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXXu" resolve="vermogen" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1GVEHS" id="6PDUWJHligm" role="1GVd_u">
        <property role="1qL7eV" value="0" />
        <property role="1Axj1u" value="true" />
        <property role="1GVIAy" value="1" />
        <property role="1GVIVt" value="SERVICE_OK" />
        <node concept="1GVH25" id="6PDUWJHlign" role="1GVH3P">
          <ref role="1GVH3K" to="4w08:6PDUWJHl8uL" resolve="besluit" />
          <node concept="27HnPa" id="6PDUWJHligo" role="27HnPl">
            <node concept="1GVH3N" id="6PDUWJHligp" role="27HnPh">
              <property role="1GVH2a" value="Recht op IIT €51" />
              <ref role="1GVH3K" to="4w08:6PDUWJHkXXC" resolve="rechtBeschrijving" />
            </node>
            <node concept="1GVH3N" id="6PDUWJHligq" role="27HnPh">
              <property role="1GVH2a" value="51" />
              <ref role="1GVH3K" to="4w08:6PDUWJHkXXH" resolve="uitTeKerenIndividueleInkomenstoeslag" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3dMsQu" id="6PDUWJHliyW" role="3dMzYz">
      <property role="TrG5h" value="Servicetest 02" />
      <node concept="3dW_9m" id="6PDUWJHliyX" role="3dLJhy">
        <property role="3dWN8O" value="2021" />
        <node concept="3dWXw4" id="6PDUWJHliyY" role="3dWWrB">
          <ref role="3dWXzV" to="4w08:6PDUWJHl8uD" resolve="invoer" />
          <node concept="27HnP5" id="6PDUWJHliyZ" role="27HnP2">
            <node concept="3dWX$1" id="6PDUWJHliz0" role="27HnPe">
              <property role="3dWX$t" value="apeldoorn" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWt" resolve="woonplaats" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz1" role="27HnPe">
              <property role="3dWX$t" value="false" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWy" resolve="aowLeeftijdBehaald" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz2" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWE" resolve="ouderDan21" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz3" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXWO" resolve="alleenstaande" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz4" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXX0" resolve="thuiswonendeKinderen" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz5" role="27HnPe">
              <property role="3dWX$t" value="1207.30" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXXe" resolve="inkomenPerMaand" />
            </node>
            <node concept="3dWX$1" id="6PDUWJHliz6" role="27HnPe">
              <property role="3dWX$t" value="12450" />
              <ref role="3dWXzV" to="4w08:6PDUWJHkXXu" resolve="vermogen" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1GVEHS" id="6PDUWJHliz7" role="1GVd_u">
        <property role="1qL7eV" value="0" />
        <property role="1Axj1u" value="true" />
        <property role="1GVIAy" value="1" />
        <property role="1GVIVt" value="SERVICE_OK" />
        <node concept="1GVH25" id="6PDUWJHliz8" role="1GVH3P">
          <ref role="1GVH3K" to="4w08:6PDUWJHl8uL" resolve="besluit" />
          <node concept="27HnPa" id="6PDUWJHliz9" role="27HnPl">
            <node concept="1GVH3N" id="6PDUWJHliza" role="27HnPh">
              <property role="1GVH2a" value="U heeft geen recht op IIT" />
              <ref role="1GVH3K" to="4w08:6PDUWJHkXXC" resolve="rechtBeschrijving" />
            </node>
            <node concept="1GVH3N" id="6PDUWJHlizb" role="27HnPh">
              <property role="1GVH2a" value="0" />
              <ref role="1GVH3K" to="4w08:6PDUWJHkXXH" resolve="uitTeKerenIndividueleInkomenstoeslag" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1rXTK1" id="6PDUWJHliJz">
    <property role="1bBDVy" value="true" />
    <property role="TrG5h" value="Bepalen individuele inkomenstoeslag" />
    <property role="3GE5qa" value="Topflow.Individuele Inkomenstoeslag" />
    <node concept="210ffa" id="3eT99K9ro$W" role="10_$IM">
      <property role="TrG5h" value="001 kopie (1)" />
      <node concept="4OhPC" id="3eT99K9ro$X" role="4Ohaa">
        <property role="TrG5h" value="ai1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3eT99K9ro$Y" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="3eT99K9ro$Z" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_0" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="3eT99K9ro_1" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_2" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="3eT99K9ro_3" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_4" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="3eT99K9ro_5" role="3_ceKu">
            <property role="3e6Tb2" value="1724,71" />
          </node>
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_6" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="3eT99K9ro_7" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_8" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="3eT99K9ro_9" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_a" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="3eT99K9ro_b" role="3_ceKu">
            <property role="3e6Tb2" value="12449" />
          </node>
        </node>
        <node concept="3_ceKt" id="3eT99K9ro_c" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
          <node concept="2JwNib" id="3eT99K9ro_d" role="3_ceKu">
            <property role="2JwNin" value="Utrecht" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3eT99K9ro_e" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3eT99K9ro_f" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
          <node concept="2JwNib" id="3eT99K9ro_g" role="3mzBi6">
            <property role="2JwNin" value="Recht op IIT €591" />
          </node>
        </node>
        <node concept="3mzBic" id="3eT99K9ro_h" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="3eT99K9ro_i" role="3mzBi6">
            <property role="3e6Tb2" value="591" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="6PDUWJHliK1" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="6PDUWJHliK2" role="4Ohaa">
        <property role="TrG5h" value="ai1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="6PDUWJHliMV" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkIiI" resolve="Aanvrager die woonachtig is in Utrecht" />
          <node concept="2Jx4MH" id="6PDUWJHliPQ" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHliMW" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGM3" resolve="Alleenstaande" />
          <node concept="2Jx4MH" id="4lzMsu9FhOA" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHliMY" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="6PDUWJHliQx" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHliN0" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH1P" resolve="Inkomen per maand" />
          <node concept="1EQTEq" id="6PDUWJHliN1" role="3_ceKu">
            <property role="3e6Tb2" value="1207,3" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHliN2" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGIg" resolve="Ouder dan 21" />
          <node concept="2Jx4MH" id="6PDUWJHliRZ" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHliN4" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGR_" resolve="Thuiswonende kinderen" />
          <node concept="2Jx4MH" id="6PDUWJHliSl" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="6PDUWJHliN6" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkH8F" resolve="Vermogen" />
          <node concept="1EQTEq" id="6PDUWJHliN7" role="3_ceKu">
            <property role="3e6Tb2" value="6225" />
          </node>
        </node>
        <node concept="3_ceKt" id="6PDUWJHliN8" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
          <node concept="2JwNib" id="6PDUWJHliN9" role="3_ceKu">
            <property role="2JwNin" value="Utrecht" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="6PDUWJHliK3" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="6PDUWJHliU0" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHfj" resolve="Recht beschrijving" />
          <node concept="2JwNib" id="6PDUWJHliU7" role="3mzBi6">
            <property role="2JwNin" value="Recht op IIT €231" />
          </node>
        </node>
        <node concept="3mzBic" id="6PDUWJHliWd" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:6PDUWJHkHxD" resolve="Uit te keren individuele inkomenstoeslag" />
          <node concept="1EQTEq" id="6PDUWJHliWn" role="3mzBi6">
            <property role="3e6Tb2" value="231" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2ljwA5" id="6PDUWJHliJ$" role="3Na4y7">
      <node concept="2ljiaL" id="6PDUWJHliJ_" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="6PDUWJHliJA" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="6PDUWJHliJB" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="3Eg$c6" id="6PDUWJHliJW" role="vfxHU">
      <ref role="3Eg$dc" to="xshh:6PDUWJHkRWZ" resolve="Bepalen individuele inkomenstoeslag" />
    </node>
  </node>
  <node concept="1rXTK1" id="3vw$xkYI1qu">
    <property role="1bBDVy" value="true" />
    <property role="3GE5qa" value="Regelgroepen.Bijdrage Sport en Cultuur" />
    <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur (18-20)" />
    <node concept="2ljwA5" id="3vw$xkYI1qv" role="3Na4y7">
      <node concept="2ljiaL" id="3vw$xkYI1qw" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="3vw$xkYI1qx" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="3vw$xkYI1qy" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="vfxHe" id="3vw$xkYI1qL" role="vfxHU">
      <ref role="vfxH2" to="dju2:3vw$xkYH054" resolve="Uit te keren bijdrage voor sport en cultuur" />
    </node>
    <node concept="210ffa" id="3vw$xkYIyXN" role="10_$IM">
      <property role="TrG5h" value="001" />
      <node concept="4OhPC" id="3vw$xkYIyY0" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIz0e" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIz0l" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIz0F" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIz11" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdYEIC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
          <node concept="2Jx4MH" id="1qYcgYdYEJw" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="1qYcgYdYFZN" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="1qYcgYdYG0H" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIz27" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIz2x" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="3vw$xkYIz2S" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIz33" role="3_ceKu">
            <property role="3e6Tb2" value="0" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIz3P" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIz42" role="3_ceKu">
            <property role="3e6Tb2" value="0" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIzmI" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIzvZ" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="1qYcgYdYJa_" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="3vw$xkYIzEw" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="3vw$xkYIzEx" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIzEy" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIzEz" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIzE$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIzE_" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIzEA" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="3vw$xkYIzEB" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIzEC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIzED" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIzEE" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIzEF" role="3_ceKu">
            <property role="3e6Tb2" value="2500" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIzEG" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIzEH" role="3_ceKu">
            <property role="3e6Tb2" value="15000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIzEI" role="4Ohb1">
        <ref role="3teO_M" node="3vw$xkYIzEx" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIzEJ" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="2CqVCR" id="3vw$xkYIPem" role="3mzBi6" />
        </node>
      </node>
    </node>
    <node concept="210ffa" id="3vw$xkYIP$Y" role="10_$IM">
      <property role="TrG5h" value="003" />
      <node concept="4OhPC" id="3vw$xkYIP$Z" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIP_0" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIP_1" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIP_2" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIP_3" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIP_4" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="3vw$xkYIP_5" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIP_6" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIP_7" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIP_8" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIP_9" role="3_ceKu">
            <property role="3e6Tb2" value="300" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIP_a" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIP_b" role="3_ceKu">
            <property role="3e6Tb2" value="10000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIP_c" role="4Ohb1">
        <ref role="3teO_M" node="3vw$xkYIP$Z" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIP_d" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="3vw$xkYIQ0O" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAhri" role="10_$IM">
      <property role="TrG5h" value="004" />
      <node concept="4OhPC" id="2bFjX$nAhrj" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAhrk" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAhrl" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAhrm" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAhrn" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAhro" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nAhrp" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAhrq" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAhrr" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAhrs" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAhrt" role="3_ceKu">
            <property role="3e6Tb2" value="750" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAhru" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAhrv" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAhrw" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nAhrj" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAhrx" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAhEK" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAiec" role="10_$IM">
      <property role="TrG5h" value="005" />
      <node concept="4OhPC" id="2bFjX$nAied" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAiee" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAief" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAieg" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAieh" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAk8F" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
          <node concept="2Jx4MH" id="2bFjX$nAk9D" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAjId" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nAjJa" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAiek" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAiel" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAiem" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAien" role="3_ceKu">
            <property role="3e6Tb2" value="605,32" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAieo" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAiep" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAieq" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nAied" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAier" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAies" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAlky" role="10_$IM">
      <property role="TrG5h" value="006" />
      <node concept="4OhPC" id="2bFjX$nAlkz" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAlk$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAlk_" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkA" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAlkB" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
          <node concept="2Jx4MH" id="2bFjX$nAlkD" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkE" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nAlkF" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkG" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAlkH" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkI" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAlkJ" role="3_ceKu">
            <property role="3e6Tb2" value="955,61" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAlkK" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAlkL" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAlkM" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nAlkz" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAlkN" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAlkO" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1rXTK1" id="3vw$xkYIQgX">
    <property role="1bBDVy" value="true" />
    <property role="3GE5qa" value="Regelgroepen.Bijdrage Sport en Cultuur" />
    <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur (21-AOW)" />
    <node concept="2ljwA5" id="3vw$xkYIQgY" role="3Na4y7">
      <node concept="2ljiaL" id="3vw$xkYIQgZ" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="3vw$xkYIQh0" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="3vw$xkYIQh1" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="vfxHe" id="3vw$xkYIQh2" role="vfxHU">
      <ref role="vfxH2" to="dju2:3vw$xkYH054" resolve="Uit te keren bijdrage voor sport en cultuur" />
    </node>
    <node concept="210ffa" id="3vw$xkYIQh3" role="10_$IM">
      <property role="TrG5h" value="001" />
      <node concept="4OhPC" id="3vw$xkYIQh4" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIQh5" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIQh6" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQh7" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
          <node concept="2Jx4MH" id="3vw$xkYIQh8" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQh9" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="3vw$xkYIQha" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhb" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIQhc" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhd" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIQhe" role="3_ceKu">
            <property role="3e6Tb2" value="1800" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhf" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIQhg" role="3_ceKu">
            <property role="3e6Tb2" value="10000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIQhh" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIQhi" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="2CqVCR" id="3vw$xkYIQMK" role="3mzBi6" />
        </node>
      </node>
    </node>
    <node concept="210ffa" id="3vw$xkYIQhk" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="3vw$xkYIQhl" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIQhm" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIQhn" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQho" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
          <node concept="2Jx4MH" id="3vw$xkYIQhp" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhq" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="3vw$xkYIQhr" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhs" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIQht" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhu" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIQhv" role="3_ceKu">
            <property role="3e6Tb2" value="2500" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhw" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIQhx" role="3_ceKu">
            <property role="3e6Tb2" value="15000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIQhy" role="4Ohb1">
        <ref role="3teO_M" node="3vw$xkYIQhl" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIQhz" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="2CqVCR" id="3vw$xkYIQh$" role="3mzBi6" />
        </node>
      </node>
    </node>
    <node concept="210ffa" id="3vw$xkYIQh_" role="10_$IM">
      <property role="TrG5h" value="003" />
      <node concept="4OhPC" id="3vw$xkYIQhA" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="3vw$xkYIQhB" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="3vw$xkYIQhC" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhD" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
          <node concept="2Jx4MH" id="3vw$xkYIQhE" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhF" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="3vw$xkYIQhG" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhH" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="3vw$xkYIQhI" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhJ" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="3vw$xkYIQhK" role="3_ceKu">
            <property role="3e6Tb2" value="300" />
          </node>
        </node>
        <node concept="3_ceKt" id="3vw$xkYIQhL" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="3vw$xkYIQhM" role="3_ceKu">
            <property role="3e6Tb2" value="10000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="3vw$xkYIQhN" role="4Ohb1">
        <ref role="3teO_M" node="3vw$xkYIQhA" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="3vw$xkYIQhO" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="3vw$xkYIQhP" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAm5e" role="10_$IM">
      <property role="TrG5h" value="004" />
      <node concept="4OhPC" id="2bFjX$nAm5f" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAm5g" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAm5h" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAm5i" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
          <node concept="2Jx4MH" id="2bFjX$nAm5j" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAm5k" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nAm5l" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAm5m" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAm5n" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAm5o" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAm5p" role="3_ceKu">
            <property role="3e6Tb2" value="1225" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAm5q" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAm5r" role="3_ceKu">
            <property role="3e6Tb2" value="6294" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAm5s" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nAm5f" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAm5t" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAm5u" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAoto" role="10_$IM">
      <property role="TrG5h" value="005" />
      <node concept="4OhPC" id="2bFjX$nAotp" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAotq" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAotr" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAots" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYH9dD" resolve="vanaf 21 tot de AOW-leeftijd" />
          <node concept="2Jx4MH" id="2bFjX$nAott" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAotu" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nAotv" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAotw" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAotx" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAoty" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAotz" role="3_ceKu">
            <property role="3e6Tb2" value="1225" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAot$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAot_" role="3_ceKu">
            <property role="3e6Tb2" value="6294" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAotA" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nAotp" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAotB" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAotC" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1rXTK1" id="1qYcgYdZkg_">
    <property role="1bBDVy" value="true" />
    <property role="3GE5qa" value="Topflow.Bijdrage Sport en Cultuur" />
    <property role="TrG5h" value="Bepalen bijdrage voor sport en cultuur" />
    <node concept="210ffa" id="1qYcgYdZin1" role="10_$IM">
      <property role="TrG5h" value="001" />
      <node concept="4OhPC" id="1qYcgYdZin2" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="43uf8IshKiU" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
          <node concept="2JwNib" id="43uf8IshKjb" role="3_ceKu">
            <property role="2JwNin" value="Amersfoort" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdZin5" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="1qYcgYdZin6" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdZlbv" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="1qYcgYdZlcd" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdZlc$" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="1qYcgYdZldk" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdZldF" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="1qYcgYdZlet" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="1qYcgYdZinb" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="1qYcgYdZinc" role="3_ceKu">
            <property role="3e6Tb2" value="875,70" />
          </node>
        </node>
        <node concept="3_ceKt" id="1qYcgYdZind" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="1qYcgYdZine" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="1qYcgYdZinf" role="4Ohb1">
        <ref role="3teO_M" node="1qYcgYdZin2" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="1qYcgYdZing" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="1qYcgYdZinh" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="43uf8Isi8yQ" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="43uf8Isi8yR" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="43uf8Isi8yS" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
          <node concept="2JwNib" id="43uf8Isi8yT" role="3_ceKu">
            <property role="2JwNin" value="amersfoort" />
          </node>
        </node>
        <node concept="3_ceKt" id="43uf8Isi8yU" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="43uf8Isi8yV" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="43uf8Isi8yW" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="43uf8Isi8yX" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="43uf8Isi8yY" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="43uf8Isi8yZ" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="43uf8Isi8z0" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="43uf8Isi8z1" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA4Yo" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
          <node concept="2Jx4MH" id="2bFjX$nA4Zi" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="43uf8Isi8z2" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="43uf8Isi8z3" role="3_ceKu">
            <property role="3e6Tb2" value="955,61" />
          </node>
        </node>
        <node concept="3_ceKt" id="43uf8Isi8z4" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="43uf8Isi8z5" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="43uf8Isi8z6" role="4Ohb1">
        <ref role="3teO_M" node="43uf8Isi8yR" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="43uf8Isi8z7" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="43uf8Isi8z8" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nA7N6" role="10_$IM">
      <property role="TrG5h" value="003" />
      <node concept="4OhPC" id="2bFjX$nA7N7" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nA7N8" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGD7" resolve="Woonplaats" />
          <node concept="2JwNib" id="2bFjX$nA7N9" role="3_ceKu">
            <property role="2JwNin" value="Apeldoorn" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Na" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGWNo" resolve="vanaf 18 tot en met 20 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nA7Nb" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Nc" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nA7Nd" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Ne" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nA7Nf" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Ng" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nA7Nh" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Ni" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHbEc" resolve="Partner jonger dan 21" />
          <node concept="2Jx4MH" id="2bFjX$nA7Nj" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Nk" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nA7Nl" role="3_ceKu">
            <property role="3e6Tb2" value="955,61" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nA7Nm" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nA7Nn" role="3_ceKu">
            <property role="3e6Tb2" value="5000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nA7No" role="4Ohb1">
        <ref role="3teO_M" node="2bFjX$nA7N7" resolve="aanvraag 1" />
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nA7Np" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="2CqVCR" id="2bFjX$nA7Va" role="3mzBi6" />
        </node>
      </node>
    </node>
    <node concept="2ljwA5" id="1qYcgYdZkgA" role="3Na4y7">
      <node concept="2ljiaL" id="1qYcgYdZkgB" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="1qYcgYdZkgC" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="1qYcgYdZkgD" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="3Eg$c6" id="1qYcgYdZkgX" role="vfxHU">
      <ref role="3Eg$dc" to="xshh:1qYcgYdZkgo" resolve="Bepalen bijdrage voor sport en cultuur" />
    </node>
  </node>
  <node concept="3dMsQ2" id="2475v2lZoDh">
    <property role="3GE5qa" value="Services.Bijdrage Sport en Cultuur" />
    <property role="3dMsO8" value="Servicetest Bijdrage Sport en Cultuur" />
    <ref role="3dMsQq" to="4w08:7J1k8s1Fzgx" resolve="BSC" />
    <node concept="3dMsQu" id="2475v2lZoPM" role="3dMzYz">
      <property role="TrG5h" value="Servicetest 01" />
      <node concept="3dW_9m" id="2475v2lZoPO" role="3dLJhy">
        <property role="3dWN8O" value="2021" />
        <node concept="3dWXw4" id="2475v2lZoPP" role="3dWWrB">
          <ref role="3dWXzV" to="4w08:7J1k8s1FBD8" resolve="invoer" />
          <node concept="27HnP5" id="2475v2lZoPQ" role="27HnP2">
            <node concept="3dWX$1" id="2475v2lZoPR" role="27HnPe">
              <property role="3dWX$t" value="Amersfoort" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F$zC" resolve="woonplaats" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPS" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_ix" resolve="vanaf18TotEnMet20Jaar" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPT" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_mN" resolve="alleenstaandeOfAlleenstaandeOuder" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPU" role="27HnPe">
              <property role="3dWX$t" value="false" />
              <ref role="3dWXzV" to="4w08:2475v2lZwz$" resolve="thuiswonendeKinderen0TM17Jaar" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPV" role="27HnPe">
              <property role="3dWX$t" value="255" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_uh" resolve="gezinsinkomen" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPW" role="27HnPe">
              <property role="3dWX$t" value="1345" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_wh" resolve="gezamenlijkVermogen" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPX" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_B3" resolve="gehuwdOfSamenwonend" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPY" role="27HnPe">
              <property role="3dWX$t" value="true" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_G5" resolve="partnerJongerDan21" />
            </node>
            <node concept="3dWX$1" id="2475v2lZoPZ" role="27HnPe">
              <property role="3dWX$t" value="false" />
              <ref role="3dWXzV" to="4w08:7J1k8s1F_Pj" resolve="vanaf21TotDeAowLeeftijd" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1GVEHS" id="2475v2lZoQ0" role="1GVd_u">
        <property role="1qL7eV" value="0" />
        <property role="1Axj1u" value="true" />
        <property role="1GVIAy" value="1" />
        <property role="1GVIVt" value="SERVICE_OK" />
        <node concept="1GVH25" id="2475v2lZoQ1" role="1GVH3P">
          <ref role="1GVH3K" to="4w08:7J1k8s1FBJQ" resolve="besluit" />
          <node concept="27HnPa" id="2475v2lZoQ2" role="27HnPl">
            <node concept="1GVH3N" id="2475v2lZoQ3" role="27HnPh">
              <property role="1GVH2a" value="90" />
              <ref role="1GVH3K" to="4w08:7J1k8s1FBA8" resolve="uitTeKerenBijdrageVoorSportEnCultuur" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1rXTK1" id="2bFjX$nAp25">
    <property role="1bBDVy" value="true" />
    <property role="3GE5qa" value="Regelgroepen.Bijdrage Sport en Cultuur" />
    <property role="TrG5h" value="Uit te keren bijdrage voor sport en cultuur (AOW-)" />
    <node concept="2ljwA5" id="2bFjX$nAp26" role="3Na4y7">
      <node concept="2ljiaL" id="2bFjX$nAp27" role="2ljwA6">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="1" />
        <property role="2ljiaM" value="1" />
      </node>
      <node concept="2ljiaL" id="2bFjX$nAp28" role="2ljwA7">
        <property role="2ljiaO" value="2021" />
        <property role="2ljiaN" value="12" />
        <property role="2ljiaM" value="31" />
      </node>
    </node>
    <node concept="2ljiaL" id="2bFjX$nAp29" role="1lUMLE">
      <property role="2ljiaO" value="2021" />
      <property role="2ljiaN" value="7" />
      <property role="2ljiaM" value="1" />
    </node>
    <node concept="vfxHe" id="2bFjX$nAp2y" role="vfxHU">
      <ref role="vfxH2" to="dju2:3vw$xkYH054" resolve="Uit te keren bijdrage voor sport en cultuur" />
    </node>
    <node concept="210ffa" id="2bFjX$nAp3Q" role="10_$IM">
      <property role="TrG5h" value="001" />
      <node concept="4OhPC" id="2bFjX$nAp3R" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAp3S" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAp3T" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp3U" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="2bFjX$nAp3V" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp3W" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nAp3X" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp7B" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nAp8g" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp3Y" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAp3Z" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp40" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAp41" role="3_ceKu">
            <property role="3e6Tb2" value="1847,63" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAp42" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAp43" role="3_ceKu">
            <property role="3e6Tb2" value="10000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAp44" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAp45" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nApjO" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nApLt" role="10_$IM">
      <property role="TrG5h" value="002" />
      <node concept="4OhPC" id="2bFjX$nApLu" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nApLv" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nApLw" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nApLx" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="2bFjX$nApLy" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nApLz" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nApL$" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nApL_" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nApLA" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nApLB" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nApLC" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nApLD" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nApLE" role="3_ceKu">
            <property role="3e6Tb2" value="1363,40" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nApLF" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nApLG" role="3_ceKu">
            <property role="3e6Tb2" value="10000" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nApLH" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nApLI" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nApLJ" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
    <node concept="210ffa" id="2bFjX$nAqo$" role="10_$IM">
      <property role="TrG5h" value="003" />
      <node concept="4OhPC" id="2bFjX$nAqo_" role="4Ohaa">
        <property role="TrG5h" value="aanvraag 1" />
        <ref role="4OhPH" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3_ceKt" id="2bFjX$nAqoA" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGW4k" resolve="Aanvrager die woonachtig is in Amersfoort" />
          <node concept="2Jx4MH" id="2bFjX$nAqoB" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoC" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:6PDUWJHkGEB" resolve="AOW leeftijd behaald" />
          <node concept="2Jx4MH" id="2bFjX$nAqoD" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoE" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHaS6" resolve="Gehuwd of Samenwonend" />
          <node concept="2Jx4MH" id="2bFjX$nAqoF" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoG" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYHa3V" resolve="Alleenstaande of alleenstaande ouder" />
          <node concept="2Jx4MH" id="2bFjX$nAqoH" role="3_ceKu">
            <property role="2Jx4MO" value="true" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoI" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:1qYcgYdYqqV" resolve="Thuiswonende kinderen 0 t/m 17 jaar" />
          <node concept="2Jx4MH" id="2bFjX$nAqoJ" role="3_ceKu" />
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoK" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGX1I" resolve="Gezinsinkomen" />
          <node concept="1EQTEq" id="2bFjX$nAqoL" role="3_ceKu">
            <property role="3e6Tb2" value="1363,40" />
          </node>
        </node>
        <node concept="3_ceKt" id="2bFjX$nAqoM" role="4OhPJ">
          <ref role="3_ceKs" to="6mfl:3vw$xkYGXjt" resolve="Gezamenlijk vermogen" />
          <node concept="1EQTEq" id="2bFjX$nAqoN" role="3_ceKu">
            <property role="3e6Tb2" value="6295" />
          </node>
        </node>
      </node>
      <node concept="4Oh8J" id="2bFjX$nAqoO" role="4Ohb1">
        <ref role="4Oh8G" to="6mfl:6PDUWJHkGCG" resolve="Aanvraag" />
        <node concept="3mzBic" id="2bFjX$nAqoP" role="4Ohbj">
          <property role="V2jGk" value="-1" />
          <ref role="10Xmnc" to="6mfl:3vw$xkYH63G" resolve="Uit te keren bijdrage voor sport en cultuur " />
          <node concept="1EQTEq" id="2bFjX$nAqoQ" role="3mzBi6">
            <property role="3e6Tb2" value="90" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

